<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Barrel
 *
 * @ORM\Table(name="barrel")
 * @ORM\Entity
 */
class Barrel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="barrelsCategory", type="string", length=255)
     */
    private $barrelsCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", options={"default" : true})
     */
    private $visible = true;

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId())
            return $this->getBarrelsCategory();
        return "Neue ton";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set barrelsCategory
     *
     * @param string $barrelsCategory
     * @return Barrel
     */
    public function setBarrelsCategory($barrelsCategory)
    {
        $this->barrelsCategory = $barrelsCategory;
    
        return $this;
    }

    /**
     * Get barrelsCategory
     *
     * @return string 
     */
    public function getBarrelsCategory()
    {
        return $this->barrelsCategory;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Barrel
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Barrel
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    public function getSelectLabel() 
    {
        return $this->getBarrelsCategory() . ' (' . $this->getDescription() . ')';
    }
}
