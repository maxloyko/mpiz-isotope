<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExternalDisposal
 *
 * @ORM\Table(name="external_disposal")
 * @ORM\Entity
 */
class ExternalDisposal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @see Category
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return ExternalDisposal
     */
    public function setCategory($category)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return ExternalDisposal
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ExternalDisposal
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return ExternalDisposal
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId())
            return "Externe Entsorgung ".$this->getId();
        return "Neue Externe Entsorgung";
    }

    /**
     * Get amount in uCi
     *
     * @return float
     */
    public function getAmountUci()
    {
        return $this->amount / 37000;
    }

    /**
     * Set amount in uCi
     *
     * @param float $amount
     * @return Order
     */
    public function setAmountUci($amount)
    {
        $this->amount = $amount * 37000;
    
        return $this;
    }
}
