<?php namespace Isotope\Bundle\DbBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="`order`")
 * @ORM\Entity(repositoryClass="Isotope\Bundle\DbBundle\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Order
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @see User
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     *
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="stock_date", type="date", nullable=true)
     *
     */
    private $stockDate;

    /**
     * @ORM\ManyToOne(targetEntity="Isotope")
     * @ORM\JoinColumn(name="isotope_id", referencedColumnName="id")
     * @see Isotope
     */
    private $isotope;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float")
     */
    private $amount;


    /**
     * @var float
     *
     * @ORM\Column(name="disposed", type="float")
     */
    private $disposed = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="status", nullable=true, options={"default" : "order"})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="costCenter", type="string", length=255, nullable=true)
     */
    private $costCenter;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", options={"default" : true})
     */
    private $visible = true;

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId()) {
            $label = $this->status;
 
            return "$label ".$this->getId();
        }
        return 'new order';
    }

    public function __construct()
    {
        if($this->date == null){
            $this->date = new \DateTime('now');
        }
        if($this->status == null){
            $this->status = 'order';
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Order
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return Order
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set status
     *
     * @param status $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return status 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set costCenter
     *
     * @param string $costCenter
     * @return Order
     */
    public function setCostCenter($costCenter)
    {
        $this->costCenter = $costCenter;
    
        return $this;
    }

    /**
     * Get costCenter
     *
     * @return string 
     */
    public function getCostCenter()
    {
        return $this->costCenter;
    }

    /**
     * Set user
     *
     * @param \Isotope\Bundle\DbBundle\Entity\User $user
     * @return Order
     */
    public function setUser(\Isotope\Bundle\DbBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \Isotope\Bundle\DbBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set isotope
     *
     * @param \Isotope\Bundle\DbBundle\Entity\Isotope $isotope
     * @return Order
     */
    public function setIsotope(\Isotope\Bundle\DbBundle\Entity\Isotope $isotope = null)
    {
        $this->isotope = $isotope;
    
        return $this;
    }

    /**
     * Get isotope
     *
     * @return \Isotope\Bundle\DbBundle\Entity\Isotope 
     */
    public function getIsotope()
    {
        return $this->isotope;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Order
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set disposed
     *
     * @param float $disposed
     * @return Order
     */
    public function addDisposed($disposed)
    {
        if ($this->getAmountLeft() < $disposed) {
            $disposed = $this->getAmountLeft();
        }

        if ($this->getAmountLeft() == $disposed) {
            $this->visible = false;
        }

        $this->disposed += $disposed;
    
        return $this;
    }

    public function delDisposed($disposed)
    {

        $left = 0;

        if ($this->disposed < $disposed) {
            $left = $disposed - $this->disposed;
            $this->disposed = 0;
        } else {
            $this->disposed -= $disposed;
        }
        $this->visible = true;

        return $left;
    }

    public function getAmountLeft() {
        return $this->amount - $this->disposed;
    }

    /**
     * Get disposed
     *
     * @return float 
     */
    public function getDisposed()
    {
        return $this->disposed;
    }

    /**
     * Set stockDate
     *
     * @param \DateTime $stockDate
     * @return Order
     */
    public function setStockDate($stockDate)
    {
        $this->stockDate = $stockDate;
    
        return $this;
    }

    /**
     * Get stockDate
     *
     * @return \DateTime 
     */
    public function getStockDate()
    {
        return $this->stockDate;
    }

    /**
     * Set disposed
     *
     * @param float $disposed
     * @return Order
     */
    public function setDisposed($disposed)
    {
        $this->disposed = $disposed;
    
        return $this;
    }

    public function getStatusLabel()
    {
        $label = $this->status;
        
        return $label;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function setStockDateOnStatus()
    {
        if(is_null($this->getStockDate()) && ($this->getStatus() == 'stock'))
        {
            $this->setStockDate(new \DateTime('today'));
        }
    }

    /**
     * Get amount in uCi
     *
     * @return float
     */
    public function getAmountUci()
    {
        return $this->amount / 37000;
    }

    /**
     * Set amount in uCi
     *
     * @param float $amount
     * @return Order
     */
    public function setAmountUci($amount)
    {
        $this->amount = $amount * 37000;
    
        return $this;
    }
}
