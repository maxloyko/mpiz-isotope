<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\Null;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="halfLife", type="float", nullable=true)
     */
    private $halfLife;

    /**
     * @var float
     *
     * @ORM\Column(name="releaseLimit", type="float", nullable=true)
     */
    private $releaseLimit;

    /**
     * @var boolean
     *
     * @ORM\Column(name="internalDisposal", type="boolean")
     */
    private $internalDisposal;

    /**
     * @var boolean
     *
     * @ORM\Column(name="visible", type="boolean", options={"default" : true})
     */
    private $visible = true;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set halfLife
     *
     * @param integer $halfLife
     * @return Category
     */
    public function setHalfLife($halfLife)
    {
        $this->halfLife = $halfLife;
    
        return $this;
    }

    /**
     * Get halfLife
     *
     * @return integer 
     */
    public function getHalfLife()
    {
        return $this->halfLife;
    }

    /**
     * Set releaseLimit
     *
     * @param float $releaseLimit
     * @return Category
     */
    public function setReleaseLimit($releaseLimit)
    {
        $this->releaseLimit = $releaseLimit;
    
        return $this;
    }

    /**
     * Get releaseLimit
     *
     * @return float 
     */
    public function getReleaseLimit()
    {
        if ($this->getInternalDisposal()) {
            return $this->releaseLimit;
        }else {
            return NULL;
        }
    }

    /**
     * Set internalDisposal
     *
     * @param boolean $internalDisposal
     * @return Category
     */
    public function setInternalDisposal($internalDisposal)
    {
        if (!$internalDisposal) {
            $this->setReleaseLimit(NULL);
        }

        $this->internalDisposal = $internalDisposal;
    
        return $this;
    }

    /**
     * Get internalDisposal
     *
     * @return boolean 
     */
    public function getInternalDisposal()
    {
        return $this->internalDisposal;
    }

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId())
            return $this->getName();
        return "Neue Kategorie";
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Category
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
