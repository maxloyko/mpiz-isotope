<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Setting
 *
 * @ORM\Table(name="setting")
 * @ORM\Entity
 */
class Setting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automatic_orders", type="boolean")
     */
    private $automaticOrders;

    /**
     * @var boolean
     *
     * @ORM\Column(name="automatic_reports", type="boolean")
     */
    private $automaticReports;

    /**
     * @var boolean
     *
     * @ORM\Column(name="maintence_mode", type="boolean")
     */
    private $maintenceMode;

    /**
     * @var string
     *
     * @ORM\Column(name="report_email", type="string", length=100)
     */
    private $reportEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="order_email", type="string", length=100)
     */
    private $orderEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="login_text_before", type="string", length=255)
     */
    private $loginTextBefore;

    /**
     * @var string
     *
     * @ORM\Column(name="login_text_after", type="string", length=255)
     */
    private $loginTextAfter;

    /**
     * @var string
     *
     * @ORM\Column(name="maintainance_msg", type="string", length=255)
     */
    private $maintainanceMsg;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set automaticOrders
     *
     * @param boolean $automaticOrders
     * @return Setting
     */
    public function setAutomaticOrders($automaticOrders)
    {
        $this->automaticOrders = $automaticOrders;

        return $this;
    }

    /**
     * Get automaticOrders
     *
     * @return boolean
     */
    public function getAutomaticOrders()
    {
        return $this->automaticOrders;
    }

    /**
     * Set automaticReports
     *
     * @param boolean $automaticReports
     * @return Setting
     */
    public function setAutomaticReports($automaticReports)
    {
        $this->automaticReports = $automaticReports;

        return $this;
    }

    /**
     * Get automaticReports
     *
     * @return boolean
     */
    public function getAutomaticReports()
    {
        return $this->automaticReports;
    }

    /**
     * Set maintenceMode
     *
     * @param boolean $maintenceMode
     * @return Setting
     */
    public function setMaintenceMode($maintenceMode)
    {
        $this->maintenceMode = $maintenceMode;

        return $this;
    }

    /**
     * Get maintenceMode
     *
     * @return boolean
     */
    public function getMaintenceMode()
    {
        return $this->maintenceMode;
    }

    /**
     * Set reportEmail
     *
     * @param string $reportEmail
     * @return Setting
     */
    public function setReportEmail($reportEmail)
    {
        $this->reportEmail = $reportEmail;

        return $this;
    }

    /**
     * Get reportEmail
     *
     * @return string
     */
    public function getReportEmail()
    {
        return $this->reportEmail;
    }

    /**
     * Set orderEmail
     *
     * @param string $orderEmail
     * @return Setting
     */
    public function setOrderEmail($orderEmail)
    {
        $this->orderEmail = $orderEmail;

        return $this;
    }

    /**
     * Get orderEmail
     *
     * @return string
     */
    public function getOrderEmail()
    {
        return $this->orderEmail;
    }

    public function __toString()
    {
        return 'Einstellungen';
    }

    /**
     * Set loginTextBefore
     *
     * @param string $loginTextBefore
     * @return Setting
     */
    public function setLoginTextBefore($loginTextBefore)
    {
        $this->loginTextBefore = $loginTextBefore;

        return $this;
    }

    /**
     * Get loginTextBefore
     *
     * @return string
     */
    public function getLoginTextBefore()
    {
        return $this->loginTextBefore;
    }

    /**
     * Set loginTextAfter
     *
     * @param string $loginTextAfter
     * @return Setting
     */
    public function setLoginTextAfter($loginTextAfter)
    {
        $this->loginTextAfter = $loginTextAfter;

        return $this;
    }

    /**
     * Get loginTextAfter
     *
     * @return string
     */
    public function getLoginTextAfter()
    {
        return $this->loginTextAfter;
    }

    /**
     * Set maintainanceMsg
     *
     * @param string $maintainanceMsg
     * @return Setting
     */
    public function setMaintainanceMsg($maintainanceMsg)
    {
        $this->maintainanceMsg = $maintainanceMsg;

        return $this;
    }

    /**
     * Get maintainanceMsg
     *
     * @return string
     */
    public function getMaintainanceMsg()
    {
        return $this->maintainanceMsg;
    }
}