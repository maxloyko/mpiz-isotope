<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InternalDisposal
 *
 * @ORM\Table(name="internal_disposal")
 * @ORM\Entity(repositoryClass="Isotope\Bundle\DbBundle\Repository\InternalDisposalRepository")
 * @ORM\HasLifecycleCallbacks
 */
class InternalDisposal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @see Category
     */
    private $category;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fillingStart", type="date", nullable=true)
     */
    private $fillingStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fillingEnd", type="date", nullable=true)
     */
    private $fillingEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualDisposal", type="datetime", nullable=true)
     */
    private $actualDisposal;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Barrel")
     * @see Barrel
     */
    private $barrelCategory;

    /**
     * @var string
     *
     * @ORM\Column(name="barrelNumber", type="string", length=16, nullable=true)
     */
    private $barrelNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="barrelPackageWeight", type="float", nullable=true)
     */
    private $barrelPackageWeight;

    /**
     * @var float
     *
     * @ORM\Column(name="totalWeight", type="float", nullable=true)
     */
    private $totalWeight;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

   /**
     * @ORM\PostLoad
     * @ORM\PrePersist
     */
    public function getCalculatedDisposalDate()
    {
        if($this->category != null && $this->totalWeight != $this->barrelPackageWeight &&
                $this->category->getReleaseLimit() > 0) {

            $desay_days = $this->getDaysBeforeReleaseCount();

            $calculatedDate = new \DateTime();
            $calculatedDate->setTimestamp(strtotime($this->date->format('Y-m-d')));
            $calculatedDate->modify('+'.intval($desay_days).' days');
            return  $calculatedDate;
        } else {
            return null;
        }
    }

    public function getDropWeight()
    {
        return ($this->totalWeight-$this->barrelPackageWeight) * 1000;
    }

    public function getSpecActivity()
    {
        return $this->amount / $this->getDropWeight();
    }

    public function getStatus()
    {
        if($this->actualDisposal === null) {
            return 'Actual';
        }
        return 'Approved';
    }

    public function getDaysBeforeReleaseCount()
    {
        return (log($this->getSpecActivity() / $this->category->getReleaseLimit()) * $this->getCategory()->getHalfLife()) / 0.6931471806;
    }

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId())
            return "Interne Entsorgung ".$this->getId();
        return "Neue Interne Entsorgung";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fillingStart
     *
     * @param \DateTime $fillingStart
     * @return InternalDisposal
     */
    public function setFillingStart($fillingStart)
    {
        $this->fillingStart = $fillingStart;
    
        return $this;
    }

    /**
     * Get fillingStart
     *
     * @return \DateTime 
     */
    public function getFillingStart()
    {
        return $this->fillingStart;
    }

    /**
     * Set fillingEnd
     *
     * @param \DateTime $fillingEnd
     * @return InternalDisposal
     */
    public function setFillingEnd($fillingEnd)
    {
        $this->fillingEnd = $fillingEnd;
    
        return $this;
    }

    /**
     * Get fillingEnd
     *
     * @return \DateTime 
     */
    public function getFillingEnd()
    {
        return $this->fillingEnd;
    }

    /**
     * Set barrelNumber
     *
     * @param string $barrelNumber
     * @return InternalDisposal
     */
    public function setBarrelNumber($barrelNumber)
    {
        $this->barrelNumber = $barrelNumber;
    
        return $this;
    }

    /**
     * Get barrelNumber
     *
     * @return string 
     */
    public function getBarrelNumber()
    {
        return $this->barrelNumber;
    }

    /**
     * Set barrelPackageWeight
     *
     * @param float $barrelPackageWeight
     * @return InternalDisposal
     */
    public function setBarrelPackageWeight($barrelPackageWeight)
    {
        $this->barrelPackageWeight = $barrelPackageWeight;
    
        return $this;
    }

    /**
     * Get barrelPackageWeight
     *
     * @return float 
     */
    public function getBarrelPackageWeight()
    {
        return $this->barrelPackageWeight;
    }

    /**
     * Set totalWeight
     *
     * @param float $totalWeight
     * @return InternalDisposal
     */
    public function setTotalWeight($totalWeight)
    {
        $this->totalWeight = $totalWeight;
    
        return $this;
    }

    /**
     * Get totalWeight
     *
     * @return float 
     */
    public function getTotalWeight()
    {
        return $this->totalWeight;
    }

    /**
     * Set amount
     *
     * @param float $amount
     * @return InternalDisposal
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    
        return $this;
    }

    /**
     * Get amount
     *
     * @return float 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return InternalDisposal
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    
        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set category
     *
     * @param \Isotope\Bundle\DbBundle\Entity\Category $category
     * @return InternalDisposal
     */
    public function setCategory(\Isotope\Bundle\DbBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Isotope\Bundle\DbBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set barrelCategory
     *
     * @param \Isotope\Bundle\DbBundle\Entity\Barrel $barrelCategory
     * @return InternalDisposal
     */
    public function setBarrelCategory(\Isotope\Bundle\DbBundle\Entity\Barrel $barrelCategory = null)
    {
        $this->barrelCategory = $barrelCategory;
    
        return $this;
    }

    /**
     * Get barrelCategory
     *
     * @return \Isotope\Bundle\DbBundle\Entity\Barrel 
     */
    public function getBarrelCategory()
    {
        return $this->barrelCategory;
    }

    public function getBarrelLabel()
    {
        $lbl = "{$this->barrelCategory} #{$this->barrelNumber}"; 
        if ($this->barrelCategory->getDescription() !== '') {
            $lbl .= " - {$this->barrelCategory->getDescription()}";
        }

        return $lbl;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return InternalDisposal
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set actualDisposal
     *
     * @param \DateTime $actualDisposal
     * @return InternalDisposal
     */
    public function setActualDisposal($actualDisposal)
    {
        $this->actualDisposal = $actualDisposal;
    
        return $this;
    }

    /**
     * Get actualDisposal
     *
     * @return \DateTime 
     */
    public function getActualDisposal()
    {
        return $this->actualDisposal;
    }

    /**
     * Get amount in uCi
     *
     * @return float
     */
    public function getAmountUci()
    {
        return $this->amount / 37000;
    }

    /**
     * Set amount in uCi
     *
     * @param float $amount
     * @return Order
     */
    public function setAmountUci($amount)
    {
        $this->amount = $amount * 37000;
    
        return $this;
    }
}
