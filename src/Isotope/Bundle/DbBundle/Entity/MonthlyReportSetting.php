<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MonthlyReportSetting
 *
 * @ORM\Table(name="monthly_report_setting")
 * @ORM\Entity
 */
class MonthlyReportSetting
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=1024)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="MpizAddres", type="string", length=255)
     */
    private $mpizAddres;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return MonthyReportSettings
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set subject
     *
     * @param string $subject
     * @return MonthyReportSettings
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return MonthyReportSettings
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set mpizAddres
     *
     * @param string $mpizAddres
     * @return MonthyReportSettings
     */
    public function setMpizAddres($mpizAddres)
    {
        $this->mpizAddres = $mpizAddres;

        return $this;
    }

    /**
     * Get mpizAddres
     *
     * @return string
     */
    public function getMpizAddres()
    {
        return $this->mpizAddres;
    }

    public function __toString()
    {
        return 'Monatsbericht Einstellungen';
    }
}
