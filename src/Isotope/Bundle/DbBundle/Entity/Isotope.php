<?php

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Isotope
 *
 * @ORM\Table(name="isotope")
 * @ORM\Entity
 */
class Isotope
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="Category")
     * @see Category
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="catalogNumber", type="string", length=15, nullable=true)
     */
    private $catalogNumber;

    /**
     * @var float
     *
     * @ORM\Column(name="rounding", type="float", nullable=true)
     */
    private $rounding;

    /**
     * @var float
     *
     * @ORM\Column(name="highestAmount", type="float", nullable=true)
     */
    private $highestAmount;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="productLine", type="string", length=255, nullable=true)
     */
    private $productLine;

    /**
     * @var string
     *
     * @ORM\Column(name="substance", type="string", length=255, nullable=true)
     */
    private $substance;

    /**
     * @var string
     *
     * @ORM\Column(name="specificActivity", type="string", length=255, nullable=true)
     */
    private $specificActivity;

    /**
     * @var string
     *
     * @ORM\Column(name="concentration", type="string", length=255, nullable=true)
     */
    private $concentration;

    /**
     * @var string
     *
     * @ORM\Column(name="deliveryType", type="string", length=255, nullable=true)
     */
    private $deliveryType;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active ", type="boolean")
     */
    private $active = true;

    /**
     * Get string name
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->getId())
            return $this->getWideName();
        return "Isotop hinzufügen";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catalogNumber
     *
     * @param string $catalogNumber
     * @return Isotope
     */
    public function setCatalogNumber($catalogNumber)
    {
        $this->catalogNumber = $catalogNumber;
    
        return $this;
    }

    /**
     * Get catalogNumber
     *
     * @return string 
     */
    public function getCatalogNumber()
    {
        return $this->catalogNumber;
    }

    /**
     * Set rounding
     *
     * @param integer $rounding
     * @return Isotope
     */
    public function setRounding($rounding)
    {
        $this->rounding = $rounding;
    
        return $this;
    }

    /**
     * Get rounding
     *
     * @return integer 
     */
    public function getRounding()
    {
        return $this->rounding;
    }

    /**
     * Set highestAmount
     *
     * @param integer $highestAmount
     * @return Isotope
     */
    public function setHighestAmount($highestAmount)
    {
        $this->highestAmount = $highestAmount;
    
        return $this;
    }

    /**
     * Get highestAmount
     *
     * @return integer 
     */
    public function getHighestAmount()
    {
        return $this->highestAmount;
    }

    /**
     * Set company
     *
     * @param string $company
     * @return Isotope
     */
    public function setCompany($company)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return string 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set productLine
     *
     * @param string $productLine
     * @return Isotope
     */
    public function setProductLine($productLine)
    {
        $this->productLine = $productLine;
    
        return $this;
    }

    /**
     * Get productLine
     *
     * @return string 
     */
    public function getProductLine()
    {
        return $this->productLine;
    }

    /**
     * Set substance
     *
     * @param string $substance
     * @return Isotope
     */
    public function setSubstance($substance)
    {
        $this->substance = $substance;
    
        return $this;
    }

    /**
     * Get substance
     *
     * @return string 
     */
    public function getSubstance()
    {
        return $this->substance;
    }

    /**
     * Set specificActivity
     *
     * @param string $specificActivity
     * @return Isotope
     */
    public function setSpecificActivity($specificActivity)
    {
        $this->specificActivity = $specificActivity;
    
        return $this;
    }

    /**
     * Get specificActivity
     *
     * @return string 
     */
    public function getSpecificActivity()
    {
        return $this->specificActivity;
    }

    /**
     * Set concentration
     *
     * @param string $concentration
     * @return Isotope
     */
    public function setConcentration($concentration)
    {
        $this->concentration = $concentration;
    
        return $this;
    }

    /**
     * Get concentration
     *
     * @return string 
     */
    public function getConcentration()
    {
        return $this->concentration;
    }

    /**
     * Set deliveryType
     *
     * @param string $deliveryType
     * @return Isotope
     */
    public function setDeliveryType($deliveryType)
    {
        $this->deliveryType = $deliveryType;
    
        return $this;
    }

    /**
     * Get deliveryType
     *
     * @return string 
     */
    public function getDeliveryType()
    {
        return $this->deliveryType;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Isotope
     */
    public function setNote($note)
    {
        $this->note = $note;
    
        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set category
     *
     * @param \Isotope\Bundle\DbBundle\Entity\Category $category
     * @return Isotope
     */
    public function setCategory(\Isotope\Bundle\DbBundle\Entity\Category $category = null)
    {
        $this->category = $category;
    
        return $this;
    }

    /**
     * Get category
     *
     * @return \Isotope\Bundle\DbBundle\Entity\Category 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Isotope
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
    
    /**
     * Get long description
     *
     * @return string 
     */
    public function getWideName() {
        return $this->productLine.' '.$this->substance.' '.
            $this->specificActivity.' '.$this->concentration.' [Nr.: '.
            $this->catalogNumber.']';
    }
    
    /**
     * Get long description for weekly report
     *
     * @return string 
     */
    public function getWideNameWeekly() {
        return $this->productLine.' '.$this->substance.', '.
            $this->specificActivity.', '.$this->concentration.' ('.
            $this->category->getName().')';
    }

    /**
     * Get highest amount in uCi
     *
     * @return float
     */
    public function getHighestAmountUci()
    {
        return $this->highestAmount / 37000;
    }
}
