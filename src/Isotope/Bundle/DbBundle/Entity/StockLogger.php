<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/14/14
 * Time: 4:09 PM
 */

namespace Isotope\Bundle\DbBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StockLogger
 * @ORM\Entity
 */

class StockLogger extends Logger {

    /**
     * @var integer
     *
     * @ORM\Column(name="isotope", type="integer")
     */
    private $isotope;

    /**
     * Set isotope
     *
     * @param integer $isotope
     * @return Stock
     */
    public function setIsotope($isotope)
    {
        $this->isotope = $isotope;
    
        return $this;
    }

    /**
     * Get isotope
     *
     * @return integer 
     */
    public function getIsotope()
    {
        return $this->isotope;
    }
}