<?php
namespace Isotope\Bundle\DbBundle\Security\User\Provider;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException,
    Symfony\Component\Security\Core\Exception\UnsupportedUserException,
    Symfony\Component\Security\Core\User\UserProviderInterface,
    Symfony\Component\Security\Core\User\UserInterface,
    Symfony\Component\DependencyInjection\ContainerInterface;

use IMAG\LdapBundle\Manager\LdapManagerUserInterface,
    IMAG\LdapBundle\User\LdapUserInterface;
use FOS\UserBundle\Model\UserManagerInterface;

class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var \IMAG\LdapBundle\Manager\LdapManagerUserInterface
     */
    private $ldapManager;

    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     */
    protected $userManager;

    /**
     * @var \Symfony\Component\Validator\Validator
     */
    protected $validator;
    
    /**
     * @var Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * Constructor
     *
     * @param LdapManagerUserInterface $ldapManager
     * @param UserManagerInterface     $userManager
     * @param Validator                $validator
     * @param ContainerInterface       $container
     */
    public function __construct(LdapManagerUserInterface $ldapManager, UserManagerInterface $userManager, $validator, ContainerInterface $container)
    {
        $this->ldapManager = $ldapManager;
        $this->userManager = $userManager;
        $this->validator = $validator;
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        // Throw the exception if the username is not provided.
        if (empty($username)) {
            throw new UsernameNotFoundException('The username is not provided.');
        }

        // check if the user is already know to us
        $user = $this->userManager->findUserBy(array("username" => $username));

        // Throw an exception if the username is not found.
        if(!$this->ldapManager->exists($username)) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found', $username));
        }

        $lm = $this->ldapManager
            ->setUsername($username)
            ->doPass();

        if (empty($user)) {
            $email = $lm->getEmail();
            
            if (empty($email)) {
                $email = $lm->getUsername();
            }
            $user = $this->userManager->createUser();
            $user->setRoles($lm->getRoles());
            $user
                ->setUsername($lm->getUsername())
                ->setPassword("")
                ->setDn($lm->getDn())
                ->setEmail($email);

            $this->userManager->updateUser($user);
        }
        

        $lmUser = $lm->getldapUser();
        $request = $this->container->get('request');
        $ldap_config = $this->container->getParameter('ldap_fields');
        if(!empty($user) && !is_null($lmUser) && strpos($request->get('_route'),'login') !== false) {
            $user->setFirstName();
            $user->setLastName();
            $user->setPhone();
            $user->setRoomNumber();
            
            if(is_array($ldap_config) && !empty($ldap_config)) {
                foreach ($ldap_config as $key => $field){
                    if(!isset($lmUser[$ldap_config[$key]])) {
                        continue;
                    }
                    switch ($key) {
                        case 'firstname':
                            $user->setFirstName($lmUser[$ldap_config['firstname']][0]);
                            break;
                        case 'lastname':
                            $user->setLastName($lmUser[$ldap_config['lastname']][0]);
                            break;
                        case 'email':
                            $user->setEmail($lmUser[$ldap_config['email']][0]);
                            break;
                        case 'phone':
                            $user->setPhone($lmUser[$ldap_config['phone']][0]);
                            break;
                        case 'roomnumber':
                            $user->setRoomNumber($lmUser[$ldap_config['roomnumber']][0]);
                            break;
                        default:
                            break;
                    }
                    
                }
                $user->setRoles($lm->getRoles());
                $this->userManager->updateUser($user);
            }
        }
        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $this->userManager->supportsClass($class);
    }
}
