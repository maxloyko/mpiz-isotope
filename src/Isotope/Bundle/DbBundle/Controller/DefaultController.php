<?php

namespace Isotope\Bundle\DbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('IsotopeDbBundle:Default:index.html.twig');
    }
}
