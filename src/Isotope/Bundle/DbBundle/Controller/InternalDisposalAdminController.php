<?php

namespace Isotope\Bundle\DbBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Isotope\Bundle\DbBundle\Entity\InternalDisposal;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Isotope\Bundle\DbBundle\Helper as Helper;

class InternalDisposalAdminController extends CRUDController
{
    
    public function approveAction($id = NULL)
    {
        $id     = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        // TODO: check permissions. Example:
        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        
        if ($this->getRestMethod() == 'APPROVE') {
            
            // check the csrf token
            $this->validateCsrfToken('sonata.approve');
            $response = new RedirectResponse($this->admin->generateUrl('list',
                    array('filter' => array(
                        'actualDisposal' => array(
                            'type' => 6,
                            'value' => array(
                                'date' => array('day' => 1, 'month' => 1, 'year' => 2009),
                                'time' => array('hour' => 0, 'minute' => 0)
                            )
                        )
                    ))
                    ) );
            try {
                if ($object->getStatus() == 'Approved') {
                    return $response;
                }

                $this->admin->setSubject($object);
                // persist if the form was valid and if in preview mode the preview was approved
                $object->setActualDisposal(new \DateTime('today'));
                $this->admin->update($object);

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array(
                        'result'    => 'ok',
                        'objectId'  => $this->admin->getNormalizedIdentifier($object)
                    ));
                }

            $this->addFlash('sonata_flash_success', $this->admin->trans('flash_edit_success', array('%name%' => $this->admin->toString($object)), 'SonataAdminBundle'));
            } catch (ModelManagerException $e) {

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans('flash_edit_error', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );
                
            }
            return $response;
        }
        return $this->render('IsotopeDbBundle:Admin:approve_internal_disposal_confirm.html.twig', array(
            'object'     => $object,
            'action'     => 'approve',
            'csrf_token' => $this->getCsrfToken('sonata.approve')
        ));
    }

    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        if ($this->getRestMethod()== 'POST') {
            $request = $this->get('request')->request;
            $data = $request->all();
            $uniqid = key($data);
            $em = $this->container->get('doctrine')->getManager();
            $category = $em->getRepository('IsotopeDbBundle:Category')->find($data[$uniqid]['category']);
            $orders = $em->getRepository('IsotopeDbBundle:Order')->filterForDispose($category);
            
            $amount = $data[$uniqid]['amount'];
            foreach ($orders as $order) {
                if ($order->getAmountLeft() >= $amount) {
                    $order->addDisposed($amount);
                    $em->persist($order);
                    $amount = 0;
                    break;
                } else {
                    $orderAmount = $order->getAmountLeft();
                    $amount -= $orderAmount;
                    $order->addDisposed($orderAmount);
                    $em->persist($order);
                }
            }

            if ($amount > 0) {
                $disposalLeft = $data[$uniqid]['amount']-$amount;

                $data[$uniqid]['amount'] = strval($disposalLeft);
                $request->set($uniqid,$data[$uniqid]);
                $this->addFlash('sonata_flash_info', "Existing amount only: ".($data[$uniqid]['amount']));
            }
        }

        return parent::createAction();
    }

    public function deleteAction($id = NULL)
    {
        $id     = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if ($this->getRestMethod() == 'DELETE') {

            if ( $object->getStatus() == 'Actual') {
                $em = $this->container->get('doctrine')->getManager();
                $orders = $em->getRepository('IsotopeDbBundle:Order')->filterForUndispose($object->getCategory());
                
                $amount = $object->getAmount();

                foreach ($orders as $order) {
                    $amount = $order->delDisposed($amount);
                    $em->persist($order);
                    if ($amount == 0) {
                        break;
                    }
                }
            } else {
                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans('flash_delete_error', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );
                return new RedirectResponse($this->admin->generateUrl('list'));
            }
        }

        return parent::deleteAction($id);

    }

    public function categorydisposalAction($id = NULL)
    {
        $category_id = $this->get('request')->get($this->admin->getIdParameter());

        $em = $this->container->get('doctrine')->getManager();
        $ordersCat = $em->getRepository('IsotopeDbBundle:Order')->filterForDispose($category_id);
        $amountCatLeft = 0;
        foreach ($ordersCat as $order)
        {
            $diff = $order->getAmountLeft();
            if($diff > 0)
            {
                $amountCatLeft += $diff;
            }
        }
        $data = json_encode(array('amount' => strval($amountCatLeft)));
        $headers = array( 'Content-type' => 'application-json; charset=utf8' );
        $responce = new Response( $data, 200, $headers );
        return $responce;
    }

    public function senddisposalAction($id)
    {
        $internal_disposal = $this->getDoctrine()->getRepository('IsotopeDbBundle:InternalDisposal')->find($id);
        if (!$internal_disposal) {
            throw $this->createNotFoundException('The Internal Disposal does not exist');
        }
        
        $reporter = new Helper\ReportHelper($this->container);

        $user = $this->get('security.context')->getToken()->getUser();
        
        $filePath = $reporter->generateDisposalPdf($internal_disposal, $user);
        $settings = $this->getDoctrine()
                    ->getRepository('IsotopeDbBundle:Setting')->find(1);
        $reportEmail = $settings->getReportEmail();
        $mailData = array(
            'subject' => 'Disposal Report ' . $id,
            'to'      => !empty($reportEmail) ? $reportEmail : '',
            'body'    => 'This is a automatic generated disposal report. ' . "\n"
            . 'Please do not response to this e-mail.' . "\n",
            'attach'  => $filePath,
        );

        $status = $reporter->sendReport($mailData);
        if ($status) {
            $this->get('session')->getFlashBag()->add(
                    'sonata_flash_success', 'Success. Email with generated pdf was sent.'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                    'sonata_flash_error', 'Error'
            );
        }
        return new RedirectResponse($this->admin->generateUrl('list'));
    }
}
