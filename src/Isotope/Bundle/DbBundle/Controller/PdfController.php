<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/8/14
 * Time: 3:12 PM
 */

namespace Isotope\Bundle\DbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PdfController extends Controller
{

    public function pdfAction($id)
    {
        $internal_disposal = $this->getDoctrine()->getRepository('IsotopeDbBundle:InternalDisposal')->find($id);
        if (!$internal_disposal) {
            throw $this->createNotFoundException('The Internal Disposal does not exist');
        }

        $usr= $this->get('security.context')->getToken()->getUser();

        $html = $this->renderView('IsotopeDbBundle:Pdf:internal_disposal.html.twig', array(
            'internal_disposal'  => $internal_disposal,
            'user' => $usr
        ));

        return new Response(
            $this->get('knp_snappy.pdf')->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
            )
        );
    }
} 