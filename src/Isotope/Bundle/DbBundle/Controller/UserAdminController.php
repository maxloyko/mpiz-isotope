<?php

namespace Isotope\Bundle\DbBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController  as Controller;

class UserAdminController extends Controller
{

    public function disableAction($id = NULL)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);
        

        if (!$object) {
            throw new NotFoundHttpException(sprintf('not found object id : %s', $id));
        }
        $object->setEnabled(false);
        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_isotope_db_user_list'));
        

    }

    public function enableAction($id = NULL)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('not found object id : %s', $id));
        }
        $object->setEnabled(true);
        $em = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();

        return $this->redirect($this->generateUrl('admin_isotope_db_user_list'));

    }
}
