<?php

namespace Isotope\Bundle\DbBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Isotope\Bundle\DbBundle\Entity\InternalDisposal;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ExternalDisposalAdminController extends CRUDController
{
    
    public function createAction()
    {
        // the key used to lookup the template
        $templateKey = 'edit';

        if (false === $this->admin->isGranted('CREATE')) {
            throw new AccessDeniedException();
        }

        if ($this->getRestMethod()== 'POST') {
            $request = $this->get('request')->request;
            $data = $request->all();
            $uniqid = key($data);
            $em = $this->container->get('doctrine')->getManager();
            $category = $orders = $em->getRepository('IsotopeDbBundle:Category')->find($data[$uniqid]['category']);
            $orders = $em->getRepository('IsotopeDbBundle:Order')->filterForDispose($category);
            
            $amount = $data[$uniqid]['amount'];

            foreach ($orders as $order) {
                if ($order->getAmountLeft() >= $amount) {
                    $order->addDisposed($amount);
                    $em->persist($order);
                    $amount = 0;
                    break;
                } else {
                    $orderAmount = $order->getAmountLeft();
                    $amount -= $orderAmount;
                    $order->addDisposed($orderAmount);
                    $em->persist($order);
                }
            }
    
            if ($amount > 0) {
                $disposalLeft = $data[$uniqid]['amount']-$amount;

                $data[$uniqid]['amount'] = strval($disposalLeft);
                $request->set($uniqid,$data[$uniqid]);
                $this->addFlash('sonata_flash_info', "Existing amount only: ".($data[$uniqid]['amount']));
            }
        }
        return parent::createAction();
    }

    public function deleteAction($id = NULL)
    {
        $id     = $this->get('request')->get($this->admin->getIdParameter());
        $object = $this->admin->getObject($id);

        if ($this->getRestMethod() == 'DELETE') {

            $em = $this->container->get('doctrine')->getEntityManager();
            $orders = $em->getRepository('IsotopeDbBundle:Order')->filterForUndispose($object->getCategory());
            
            $amount = $object->getAmount();

            foreach ($orders as $order) {
                $amount = $order->delDisposed($amount);
                $em->persist($order);
                if ($amount == 0) {
                    break;
                }
            }
        }

        return parent::deleteAction($id);

    }
}
