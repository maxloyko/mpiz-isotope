<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/8/14
 * Time: 3:12 PM
 */

namespace Isotope\Bundle\DbBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class OrderController extends CRUDController
{

    public function deleteAction($id)
    {

        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('DELETE', $object)) {
            throw new AccessDeniedException();
        }

        if ($this->getRestMethod() == 'DELETE') {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            try {
                $status = $object->getStatus();
                switch ($status) {
                    case 'order':
                        $status = 'deleted (order)';
                        break;
                    case 'stock':
                        $status = 'deleted (stock)';
                        break;
                }

                $object->setStatus($status);
                $object->setVisible(FALSE);

                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'ok'));
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans('flash_delete_success', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );

            } catch (ModelManagerException $e) {

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans('flash_delete_error', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );
            }

            return new RedirectResponse($this->generateUrl('admin_isotope_db_order_list'));
        }

        return $this->render($this->admin->getTemplate('delete'), array(
            'object'     => $object,
            'action'     => 'delete',
            'csrf_token' => $this->getCsrfToken('sonata.delete')
        ));
    }

    
    
    /**
     * redirect the user depend on this choice
     *
     * @param object $object
     *
     * @return Response
     */
    protected function redirectTo($object)
    {
        
        $status = $object->getStatus();
        switch ($status) {
            case 'order':
                $get = '?filter[status][value]=order';
                break;
            case 'stock':
                $get = '?filter[status][value]=stock';
                break;
        }
        
        $url = false;

        if ($this->get('request')->get('btn_update_and_list') !== null) {
            $url = $this->admin->generateUrl('list').$get;
        }
        if ($this->get('request')->get('btn_create_and_list') !== null) {
            $url = $this->admin->generateUrl('list').$get;
        }

        if ($this->get('request')->get('btn_create_and_create') !== null) {
            $params = array();
            if ($this->admin->hasActiveSubClass()) {
                $params['subclass'] = $this->get('request')->get('subclass');
            }
            $url = $this->admin->generateUrl('create', $params);
        }

        if (!$url) {
            $url = $this->admin->generateObjectUrl('edit', $object);
        }

        return new RedirectResponse($url);
    }
} 