<?php

namespace Isotope\Bundle\DbBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Isotope\Bundle\DbBundle\Entity\Isotope;
use Symfony\Component\HttpFoundation\RedirectResponse;

class IsotopeAdminController extends CRUDController
{
    public function hideAction($id = NULL)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('DELETE', $object)) {
            throw new AccessDeniedException();
        }

        if ($this->getRestMethod() == 'DELETE') {
            // check the csrf token
            $this->validateCsrfToken('sonata.delete');

            try {
                $object->setActive(false);
                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'ok'));
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans('flash_delete_success', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );

            } catch (ModelManagerException $e) {

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans('flash_delete_error', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );
            }

            return new RedirectResponse($this->generateUrl('admin_isotope_db_isotope_list'));
        }

        return $this->render($this->admin->getTemplate('hide'), array(
            'object'     => $object,
            'action'     => 'hide',
            'csrf_token' => $this->getCsrfToken('sonata.delete')
        ));
    }

    public function reactivateAction($id = NULL)
    {
        $id = $this->get('request')->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id : %s', $id));
        }

        if (false === $this->admin->isGranted('EDIT', $object)) {
            throw new AccessDeniedException();
        }
        if ($this->getRestMethod() == 'GET') {
            // check the csrf token
//            $this->validateCsrfToken('sonata.delete');

            try {
                $object->setActive(true);
                $em = $this->getDoctrine()->getManager();
                $em->persist($object);
                $em->flush();

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'ok'));
                }

                $this->addFlash(
                    'sonata_flash_success',
                    $this->admin->trans('flash_edit_success', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );

            } catch (ModelManagerException $e) {

                if ($this->isXmlHttpRequest()) {
                    return $this->renderJson(array('result' => 'error'));
                }

                $this->addFlash(
                    'sonata_flash_error',
                    $this->admin->trans('flash_edit_error', array('%name%' => $this->admin->toString($object)),
                    'SonataAdminBundle')
                );
            }

            return new RedirectResponse($this->generateUrl('admin_isotope_db_isotope_list').'?filter[active][value]=1');
        }
        return new RedirectResponse($this->admin->generateUrl('list').'?filter[active][value]=1');
    }
}
