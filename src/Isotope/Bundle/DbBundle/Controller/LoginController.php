<?php
/**
 * Created by PhpStorm.
 * User: Dymok
 * Date: 12/25/13
 * Time: 12:53 PM
 */

namespace Isotope\Bundle\DbBundle\Controller;

use IMAG\LdapBundle\Controller\DefaultController as LdapController;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends LdapController
{

    public function loginAction()
    {
        $error = $this->getAuthenticationError();
        $em = $this->container->get('doctrine')->getManager();
        $settings = $em->getRepository('IsotopeDbBundle:Setting')->find(1);
        $loginTextBefore = $settings->getLoginTextBefore();
        $loginTextAfter = $settings->getLoginTextAfter();
        $maintainanceMode = $settings->getMaintenceMode();
        $maintainanceMsg = $settings->getMaintainanceMsg();
        $lastUsername = $this->get('request')->getSession()->get(SecurityContext::LAST_USERNAME);
        $cookies = $this->get('request')->cookies->all();
        $maintain = $userRole = $username = null;

        $username = $this->get('security.context')->getToken()->getUser();
        if(isset($cookies['not_user_role'])){
            $userRole = $cookies['not_user_role'];
        }
        if(isset($cookies['maintain'])){
            $maintain = $cookies['maintain'];
        }
        $credentials = false;
        if(!is_null($error)) {
            $errorType = explode("\\",get_class($error));
            $credentials = ($errorType[count($errorType) - 1] == 'BadCredentialsException') ? true : false ;

        }

        $response =  $this->render('IsotopeDbBundle:Default:login.html.twig', array(

            'text_before'   => !is_null($loginTextBefore) ? $loginTextBefore : '',
            'last_username' => $lastUsername ? $lastUsername: ($username && ($username != 'anon.') && ($userRole || $maintain)) ? $username : null,
            'error'         => $error,
            'token'         => $this->generateToken(),

            'text_after'    => !is_null($loginTextAfter) ? $loginTextAfter : '',
            'maintainance_mode' => $maintainanceMode,
            'maintainance_msg' => !is_null($maintainanceMsg) ? $maintainanceMsg : '',
            'not_user_role' => $userRole,
            'credentials' => $credentials,
            'maintain' => $maintain
        ));
        $response->headers->clearCookie('maintain');
        $response->headers->clearCookie('not_user_role');

        return $response;
    }

} 