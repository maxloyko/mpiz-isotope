<?php
/**
 * Created by PhpStorm.
 * User: Max Loyko
 * Date: 12/27/13
 * Time: 6:01 PM
 */

namespace Isotope\Bundle\DbBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Isotope\Bundle\DbBundle\Helper as Helper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends Controller
{

    public function balanceYearAction(Request $request)
    {
        $columns = array(
            'category',
            'init_invent',
            'purchased',
            'disposed',
            'difference',
            'end_stock',
        );
        $date = new \DateTime('today');
        $reporter = new Helper\ReportHelper($this->container);

        $formDate = $this->createFormBuilder()
            ->add('date', 'date', array(
                'data' => $date,
                'format' => 'dd-MMMM-yyyy',
                'years' => range(2005, date('Y')),
                'input' => 'datetime',
                'widget' => 'choice',
                ))
            ->add('submit', 'submit')
            ->add('reportxls', 'submit',array('attr' => array('value' => 'true'),
                'label' => 'xls report'))
                ->getForm();

        $viewData = array(
                'form'    => $formDate->createView(),
                'columns' => $columns,
            );
        if ($request->isMethod('POST')) {
            $formData = $request->request->get('form');
            if (!is_null($formData['date'])) {
                $date->setDate($formData['date']['year'], '01', '01');
            }
            $formDate->handleRequest($request);
            $viewData['form'] = $formDate->createView();
            $dataNew = $reporter->getBalanceYear($date);
            $viewData['data'] = $dataNew;
            if (isset($formData['reportxls']) && $formData['reportxls'] == 'true') {
                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
                $row = 1;
                $title_excel = 'Jahrensbilanzen';
                $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A1', 'Isotop')
                        ->setCellValue('B1', 'Ausgangs Bestand Bq (µCi)')
                        ->setCellValue('C1', 'Eingekauft Bq (µCi)')
                        ->setCellValue('D1', 'Entsorgt Bq (µCi)')
                        ->setCellValue('E1', 'Differenz Bq (µCi)')
                        ->setCellValue('F1', 'End-Bestand Bq (µCi)')
                ;
                foreach ($dataNew as $c) {
                    $row++;
                    $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $row, $c['category'])
                            ->setCellValue('B' . $row, $c['initial_stock'] > 0 ? sprintf("%.2e (%d)", $c['initial_stock'], $c['initial_stock_ci']) : '0' )
                            ->setCellValue('C' . $row, $c['purchased'] > 0 ? sprintf("%.2e (%d)", $c['purchased'], $c['purchased_ci']) : '0')
                            ->setCellValue('D' . $row, $c['disposed'] > 0 ? sprintf("%.2e (%d)", $c['disposed'], $c['disposed_ci']) : '0')
                            ->setCellValue('E' . $row, $c['difference'] > 0 ? sprintf("%.2e (%d)", $c['difference'], $c['difference_ci']) : '0')
                            ->setCellValue('F' . $row, $c['end_stock'] > 0 ? sprintf("%.2e (%d)", $c['end_stock'], $c['end_stock_ci']) : '0')
                    ;
                }


                $phpExcelObject->getProperties()->setCreator("liuggio")
                        ->setLastModifiedBy("Giulio De Donato")
                        ->setTitle("Office 2005 XLSX Test Document")
                        ->setSubject("Office 2005 XLSX Test Document")
                        ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
                        ->setKeywords("office 2005 openxml php")
                        ->setCategory("Test result file");

                $phpExcelObject->getActiveSheet()->setTitle(ucfirst($title_excel));
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                // create the writer
                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
                // create the response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                // adding headers
                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment;filename=' . $title_excel . '-' . $formData['date']['year'] . '.xls');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');

                return $response;
            }
        } else {
            $date->setDate(date('Y'), '01', '01');
            $data = $reporter->getBalanceYear($date);
            $viewData['data'] = $data;
        }
        return $this->render('IsotopeDbBundle:Admin:balance_year.html.twig', $viewData);
    }
    public function balanceAction(Request $request)
    {
        $columns = array(
            'category',
            'init_invent',
            'purchased',
            'disposed',
            'difference',
            'end_stock',
        );
        $date = new \DateTime('today');
        $reporter = new Helper\ReportHelper($this->container);

        $formDate = $this->createFormBuilder()
            ->add('date', 'date', array(
                'data' => $date,
                'format' => 'dd-MMMM-yyyy',
                'years' => range(2005, date('Y')),
                'input' => 'datetime',
                'widget' => 'choice',
                ))
            ->add('submit', 'submit')
            ->add('report', 'submit',array('attr' => array('value' => 'true'),
                'label' => 'pdf report'))
            ->add('mail', 'submit',array('attr' => array('value' => 'true'),
                'label' => 'Send report'))
            ->getForm();

        $viewData = array(
                'form'    => $formDate->createView(),
                'columns' => $columns,
            );
        if ($request->isMethod('POST')) {
            $formData = $request->request->get('form');
            if (!is_null($formData['date'])) {
                if ($formData['date']['year'] == $date->format('Y') && intval($formData['date']['month']) <= intval($date->format('n'))) {
                    $date->setDate($formData['date']['year'], $formData['date']['month'], '01');

                } elseif ($formData['date']['year'] == $date->format('Y') && intval($formData['date']['month']) > intval($date->format('n'))) {
                    $date->setDate($formData['date']['year'], $date->format('n'), '01');
                    $formData['date']['month'] = $date->format('n');
                    
                    $request->request->set('form', $formData);
                } else {
                    $date->setDate($formData['date']['year'], $formData['date']['month'], '01');
                }
            }
            $formDate->handleRequest($request);
            $viewData['form'] = $formDate->createView();

            $dataNew = $reporter->getBalance($date);
            $viewData['data'] = $dataNew;
            if ((isset($formData['report']) && $formData['report'] == 'true')
                    || (isset($formData['mail']) && $formData['mail'] == 'true')) {
                $settingsMonthRep = $this->getDoctrine()
                                ->getRepository('IsotopeDbBundle:MonthlyReportSetting')->find(1);
                
                $filename = $reporter->generateBalancePdf($dataNew, $date, $settingsMonthRep);
                if (isset($formData['mail']) && $formData['mail'] == 'true') {
                    $settings = $this->getDoctrine()
                                ->getRepository('IsotopeDbBundle:Setting')->find(1);
                    $reportEmail = explode(',', $settings->getReportEmail());
                    $mailData = array(
                        'subject' => 'Balance Report ' . $date->format('Y') . ' ' . $date->format('m'),
                        'to'      => !empty($reportEmail) ? $reportEmail : '',
                        'body'    => 'This is a automatic generated monthly report. ' . "\n"
                        . 'Please do not response to this e-mail.' . "\n",
                        'attach'  => $filename,
                    );

                    $status = $reporter->sendReport($mailData);
                    if ($status) {
                        $this->get('session')->getFlashBag()->add(
                                'sonata_flash_success', 'Success. Email with generated pdf was sent.'
                        );
                    } else {
                        $this->get('session')->getFlashBag()->add(
                                'sonata_flash_error', 'Error'
                        );
                    }
                } else {
                    $response = new Response();

                        return new Response(
                            file_get_contents($filename),
                            200,
                            array(
                                'Content-Type'          => 'application/pdf',
                            )
                        );
                    return $response;
                }
                
            }
        } else {
            $data = $reporter->getBalance($date);
            $viewData['data'] = $data;
        }
        return $this->render('IsotopeDbBundle:Admin:balance.html.twig', $viewData);
    }

    public function sendWeeklyReportAction()
    {

        $reporter = new Helper\ReportHelper($this->container);
        $status = $reporter->sendWeeklyReport();

        if ($status) {
            $this->get('session')->getFlashBag()->add(
                'sonata_flash_success',
                'Success'
            );
        } else {
            $this->get('session')->getFlashBag()->add(
                'sonata_flash_error',
                'Error'
            );
        }
        return $this->redirect($this->generateUrl('admin_isotope_db_setting_edit', array('id' =>1)));
    }

    public function annualReportsAction(Request $request)
    {
        $columns = array(
            'Isotop',
            'Erwerb (µCi)',
            'Abgabe (µCi)',
        );

        $reporter = new Helper\ReportHelper($this->container);
        $date = new \DateTime();
        $date->modify('-1 year');
        $result = $date->format('Y');
        $annual_reports = $reporter->getAnnualBalance($date);
        $disposals =  $this->getDoctrine()->getRepository('IsotopeDbBundle:InternalDisposal')->filterByAnnual($date);
        $corrections = $reporter->getAnnualCorrection($date);


        $form = $this->createFormBuilder()
            ->add('year', 'choice', array(
                'data' =>  $result,
                'choices' => array_combine(range(2005, date('Y')),range(2005, date('Y'))),
                'label' => false

            ))
            ->add('save', 'submit', array('attr' => array('class' => 'btn span2'),
                'label' => 'Jahr auswählen'))
            ->add('totals', 'submit',array(
                'attr' => array('value' => 'true',
                    'class' => 'btn span2'),
                'label' => 'Gesamtsummen (XLS)'))
            ->add('balance', 'submit',array(
                'attr' => array('value' => 'true',
                    'class' => 'btn span2'),
                'label' => 'Jahresbilanz (XLS)'))
            ->add('corrections', 'submit',array(
                'attr' => array('value' => 'true',
                    'class' => 'btn span2'),
                'label' => 'Korrekturen (XLS)'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $date = new \DateTime();
            $date = $date->setDate($data['year'], 1, 1);
            $annual_reports = $reporter->getAnnualBalance($date);
            $disposals =  $this->getDoctrine()->getRepository('IsotopeDbBundle:InternalDisposal')->filterByAnnual($date);
            $corrections = $reporter->getAnnualCorrection($date);

            if (!$form->get('save')->isClicked() && ($form->get('totals')->isClicked()
                || $form->get('balance')->isClicked()
                || $form->get('corrections')->isClicked())) {

                $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject();
                $row = 1;

                if ($form->get('totals')->isClicked()) {

                    $title_excel = 'Gesamtsummen-Erwerb-und-Abgabe';
                    $date = new \DateTime();
                    $date = $date->setDate($data['year'], 1, 1);
                    $annual_reports = $reporter->getAnnualBalance($date);
                    $cols = array('A','B','C');
                    foreach ($columns as $k=>$c)
                    {
                        $phpExcelObject->setActiveSheetIndex(0)

                            ->setCellValue($cols[$k]. '1', $c)
                        ;
                    }

                    foreach ($annual_reports as $k=>$a_r) {
                        $row++;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A'. $row, $a_r['category'])
                            ->setCellValue('B'. $row, trim($a_r['purchased_ci'], '-'))
                            ->setCellValue('C'. $row, trim($a_r['disposed_ci'], '-'))
                        ;
                    }

                }elseif ($form->get('balance')->isClicked()) {

                    $title_excel = 'Jahresbilanz';

                    $titles = array(
                        'Nuklid',
                        'Art des Abfalls',
                        'Ausgangs-aktivität des kontami-nierenden Nuklids in Bq',
                        'Masse in g',
                        'Datum der Einlagerung',
                        'Datum der Freigabe',
                        'Spezifische Aktivität in Bq/g',
                        'Freigabe-grenzwert in Bq/g',
                        'Halbwerts-zeit in Tagen',
                        'Berechnete Abklingzeit in Tagen',
                        'Berechnetes Freigabedatum'


                    );

                    $cols = array('A','B','C','D','E','F','G','H','I','J','K');
                    foreach ($cols as $k=>$c){
                        if ($k < 6){
                        $phpExcelObject->setActiveSheetIndex(0)->
                            mergeCells($c . '1:'. $c .'2');
                        }
                    }

                    //Set merge big column
                    $phpExcelObject->setActiveSheetIndex(0)
                        ->mergeCells('G1:K1')
                        ->setCellValue('G1', 'Annahmen und Ergebnis des Freigabenachweises');


                    //set title
                    $cols_merge = array('G','H','I','J','K');
                    foreach ($titles as $k=>$t) {

                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue(in_array($cols[$k], $cols_merge)  ? "$cols[$k]2" : "$cols[$k]1", $t)
                        ;
                    }

                    $row +=1;
                    foreach ($disposals as $d) {
                        $row++;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A' . $row, $d->getCategory()->getName())
                            ->setCellValue('B' . $row, $d->getbarrelCategory()->getDescription())
                            ->setCellValue('C' . $row, $d->getAmount())
                            ->setCellValue('D' . $row, $d->getDropWeight())
                            ->setCellValue('E' . $row, \PHPExcel_Shared_Date::PHPToExcel($d->getdate()))
                            ->setCellValue('F' . $row, \PHPExcel_Shared_Date::PHPToExcel($d->getActualDisposal()))
                            ->setCellValue('G' . $row, $d->getSpecActivity())
                            ->setCellValue('H' . $row, $d->getCategory()->getReleaseLimit())
                            ->setCellValue('I' . $row, $d->getCategory()->getHalfLife())
                            ->setCellValue('J' . $row, trim($d->getDaysBeforeReleaseCount(), '-'))
                            ->setCellValue('K' . $row, \PHPExcel_Shared_Date::PHPToExcel($d->getCalculatedDisposalDate()));

                            $phpExcelObject->setActiveSheetIndex(0)
                                ->getStyle('C' . $row)->getNumberFormat()->setFormatCode('0.00E+#');
                            $phpExcelObject->setActiveSheetIndex(0)
                                ->getStyle('E' . $row)->getNumberFormat()->setFormatCode('dd.mm.yyyy');
                             $phpExcelObject->setActiveSheetIndex(0)
                                ->getStyle('F' . $row)->getNumberFormat()->setFormatCode('dd.mm.yyyy');
                             $phpExcelObject->setActiveSheetIndex(0)
                                ->getStyle('K' . $row)->getNumberFormat()->setFormatCode('dd.mm.yyyy');

                    }
                }elseif ($form->get('corrections')->isClicked()) {

                    $title_excel = 'Korrekturen';

                    $phpExcelObject->setActiveSheetIndex(0)
                        ->setCellValue('A1','Isotop')
                        ->setCellValue('B1','Menge (uCi)')
                        ->setCellValue('C1','Datum')
                        ;

                    foreach($corrections as $c) {
                        $row++;
                        $phpExcelObject->setActiveSheetIndex(0)
                            ->setCellValue('A'.$row, $c['category'])
                            ->setCellValue('B'.$row,trim($c['amount_ci'], '-'))
                            ->setCellValue('C'.$row,\PHPExcel_Shared_Date::PHPToExcel(strtotime(trim($c['date'], '-'))))
                            ;
                         $phpExcelObject->setActiveSheetIndex(0)
                            ->getStyle('C' . $row)->getNumberFormat()->setFormatCode('dd.mm.yyyy');
                    }
                }

                $phpExcelObject->getProperties()->setCreator("liuggio")
                    ->setLastModifiedBy("Giulio De Donato")
                    ->setTitle("Office 2005 XLSX Test Document")
                    ->setSubject("Office 2005 XLSX Test Document")
                    ->setDescription("Test document for Office 2005 XLSX, generated using PHP classes.")
                    ->setKeywords("office 2005 openxml php")
                    ->setCategory("Test result file");

                $phpExcelObject->getActiveSheet()->setTitle(ucfirst($title_excel));
                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $phpExcelObject->setActiveSheetIndex(0);

                // create the writer
                $writer = $this->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');
                // create the response
                $response = $this->get('phpexcel')->createStreamedResponse($writer);
                // adding headers
                $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment;filename='.$title_excel.'-'.$data['year'].'.xls');
                $response->headers->set('Pragma', 'public');
                $response->headers->set('Cache-Control', 'maxage=1');

                return $response;

            }
        }

        return $this->render('IsotopeDbBundle:Admin:annual_reports.html.twig', array(
            'form' => $form->createView(),
            'columns' => $columns,
            'data' => $annual_reports,
            'disposals' => $disposals,
            'corrections' => $corrections
        ));
    }
}
