<?php

namespace Isotope\Bundle\DbBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Isotope\Bundle\DbBundle\Entity as Entity;

use PHPExcel;
use PHPExcel_IOFactory;

class MigrateDataCommand extends ContainerAwareCommand
{

    private $_input;
    private $_output;
    private $_importer;

    private $_dummy_order_id = 1500;
    private $_cache = array();

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {

        $this
            ->setName('mpiz:data:migrate')
            ->setDescription('Migrate old data')
            ->addOption(
                'path',
                'p',
                InputOption::VALUE_OPTIONAL,
                'Path to the directory with files'
            )
        ;

    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->_input = $input;
        $this->_output = $output;
        $this->_dialog = $this->getHelperSet()->get('dialog');

        $entities = array(
            array(
                'file' => 'category',
                'func' => 'addCategory'
            ),
            array(
                'file' => 'isotope',
                'func' => 'addIsotope'
            ),
            array(
                'file' => 'barrel',
                'func' => 'addBarrel'
            ),
            array(
                'file' => 'user',
                'func' => 'addUser'
            ),
            array(
                'file' => 'order',
                'func' => 'addOrder'
            ),
            array(
                'file' => 'stock',
                'func' => 'addStock'
            ),

            array(
                'file' => 'disposal',
                'func' => 'addDisposal'
            ),

            array(
                'file' => 'settings',
                'func' => 'updateSettings'
            )
        );

        $this->cleanUp();

        foreach ($entities as $this->_importer) {
            if (($status = $this->import()) !== 'ok') {
                $this->handleWarning($status);
                continue;
            }
        }
        $this->usernameMigrate();
        $this->_output->writeln('<info>done</info>');

    }
    
    private function import()
    {
        
        $filename = $this->_importer['file'] . '.csv';

        $data = $this->parseCSV($filename);

        if($data === false) {
            $this->handleError('CSV parse error');
        }

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->getConnection()->beginTransaction();

        foreach($data as $item) {
            call_user_method($this->_importer['func'], $this, $item);
        }

        try {
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
            return $e->getMessage();
        }

        $em->flush();
        $em->getConnection()->commit();
        return 'ok';
    }

    private function cleanUp() {
        $delete = array(
            'IsotopeDbBundle:ExternalDisposal',
            'IsotopeDbBundle:InternalDisposal',
            'IsotopeDbBundle:Order',
            'IsotopeDbBundle:Isotope',
            'IsotopeDbBundle:Category',
            'IsotopeDbBundle:Barrel',
            'IsotopeDbBundle:User',
            'IsotopeDbBundle:Logger',
            'IsotopeDbBundle:Setting'
        );

        foreach($delete as $entity) { 
            $qb = $this->getContainer()->get('doctrine')->getEntityManager()->createQueryBuilder();
            $qb->delete($entity)
                        ->getQuery()
                        ->execute();
        }
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->flush();

    }

    private function addCategory($oc)
    {

        $c = new Entity\Category();
        $c->setName($oc['category_name']);
        $c->setHalfLife($oc['halflife']);
        $c->setReleaseLimit($oc['approval']);
        $c->setInternalDisposal(true);
        $visible = $oc['category_visible'] == 't';
        $c->setVisible($visible);
        $this->setIdForObj($c, $oc['category_id']);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($c);

    }

    private function addIsotope($oi) {

        $i = new Entity\Isotope();

        $this->setIdForObj($i, $oi['isotope_id']);

        $i->setCatalogNumber($oi['catalog_number']);
        $i->setRounding($oi['round'] * 37000);
        $i->setHighestAmount($oi['order_limit'] * 37000);
        $i->setCompany($oi['company']);
        $i->setProductLine($oi['product_line']);
        $i->setSubstance($oi['substance']);
        $i->setSpecificActivity($oi['specific_activity']);
        $i->setConcentration($oi['concentration']);
        $i->setDeliveryType($oi['delivery']);
        $i->setNote($oi['notice']);
        $active = $oi['isotope_visible'] == 't';
        $i->setActive($active);
        
        $category = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Category')->find($oi['category_id']);
        $i->setCategory($category);

        $internal = $oi['disposal'] == 't';
        //$category->setInternalDisposal($internal);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        //$em->persist($category);
        $em->persist($i);
    }

    private function addBarrel($ob)
    {

        $b = new Entity\Barrel();
        $b->setBarrelsCategory($ob['barrel_category']);
        $b->setDescription($ob['barrel_desc']);
        $visible = $ob['visible'] == 't';
        $b->setVisible($visible);
        $this->setIdForObj($b, $ob['barrel_id']);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($b);

    }


    private function addUser($ou)
    {

//var_dump($ou);
        $u = new Entity\User();
        $u->setDn($ou['username']);
        $u->setUsername($ou['username']);
        $u->setUsernameCanonical($ou['username']);
        $u->setFirstName($ou['first_name']);
        $u->setLastName($ou['sure_name']);
        $u->setPhone($ou['phone']);
        $email = $ou['user_id'] . '@dummy.de';
        $u->setEmail($email);
//        $u->setEmailCanonical($email);
        $account = $this->getFromCache('account', 'account_id', $ou['account_id']);
        $u->setPassword($ou['user_password']);
        $enabled = $ou['user_visible'] == 't';
        $u->setEnabled($enabled);
        $this->setIdForObj($u, $this->convertUserId($ou['user_id']));

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($u);

    }

    private function addOrder($oo)
    {

        $o = new Entity\Order();
        $date = new \DateTime($oo['order_time']);
        $o->setDate($date);
        $o->setAmountUci($oo['amount']);
        $user = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:User')->find($this->convertUserId($oo['user_id']));
        $ac = $this->getFromCache('account', 'account_id', $oo['account_id']);
        $o->setCostCenter($ac['account_name']);
        $o->setUser($user);
        $isotope = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Isotope')->find($oo['isotope_id']);
        $o->setIsotope($isotope);
        $visible = $oo['order_visible'] == 't';
        $o->setVisible($visible);
        $status = ($visible) ? 'order' : 'deleted (order)';
        $o->setStatus($status);
        $this->setIdForObj($o, $oo['order_id']);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($o);
    }

    private function addStock($os)
    {

        $visible = $os['stock_visible'] == 't';

        if (!$visible) {
            return;
        }

        $us = $this->getFromCache('user_stock', 'stock_id', $os['stock_id']);

        $o = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Order')->find($us['order_id']);
        if (empty($o) || $o->getStatus() == 'stock' || $o->getStatus() == 'deleted (stock)') {
            $o = new Entity\Order();
            $date = new \DateTime($us['stock_time']);
            $o->setDate($date);
            $user = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:User')->find($this->convertUserId($us['user_id']));
            $o->setUser($user);
            $isotope = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Isotope')->find($os['isotope_id']);
            $o->setIsotope($isotope);
            $ou = $this->getFromCache('user', 'user_id', $us['user_id']);
            $ac = $this->getFromCache('account', 'account_id', $ou['account_id']);
            $o->setCostCenter($ac['account_name']);
            $this->setIdForObj($o, $this->_dummy_order_id);
            $this->_dummy_order_id++;
        }

        $o->setAmountUci($os['amount']);
        $o->setVisible($visible);
        $status = ($os['stock_visible'] == 't') ? 'stock' : 'deleted (stock)';
        $o->setStatus($status);
        $date = new \DateTime($us['stock_time']);
        $o->setStockDate($date);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($o);
    }

    private function addDisposal($od)
    {

        $ds_items = $this->getFromCache('disposal_stock', 'disposal_id', $od['disposal_id']);

        if (!$ds_items) {
            return;
        }

        $amount = 0;
        $i = null;
        foreach($ds_items as $ds) {
            $s = $this->getFromCache('stock', 'stock_id', $ds['stock_id']);
            if (is_null($i)) {
                $i = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Isotope')->find($s['isotope_id']);
            }
            $amount += $ds['amount'] * 37000;
        }

        $d = new Entity\InternalDisposal();
        $date = new \DateTime($od['disposal_time']);
        $d->setDate($date);

        if (!empty($od['approval_time'])) {
            $date = new \DateTime($od['approval_time']);
            $d->setActualDisposal($date);
        }

        $date = new \DateTime($od['begin_filling_time']);
        $d->setFillingStart($date);
        $date = new \DateTime($od['end_filling_time']);
        $d->setFillingEnd($date);
        $d->setBarrelNumber($od['barrel_number']); 
        $b = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:Barrel')->find($od['barrel_id']);
        $d->setBarrelCategory($b);
        $d->setBarrelPackageWeight($od['barrel_weight']);
        $d->setTotalWeight($od['weight']);
        $n = $this->getFromCache('dis_notice', 'dis_notice_id', $od['dis_notice_id']);
        $d->setComment($n['notice']);

        $this->setIdForObj($d, $od['disposal_id']);
        $d->setCategory($i->getCategory());
        $d->setAmount($amount);
        
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($d);

        $orders = $em->getRepository('IsotopeDbBundle:Order')->filterForDispose($i->getCategory());
        
        foreach ($orders as $order) {
            if ($order->getAmountLeft() >= $amount) {
                $order->addDisposed($amount);
                $em->persist($order);
                $amount = 0;
                break;
            } else {
                $orderAmount = $order->getAmountLeft();
                $amount -= $orderAmount;
                $order->addDisposed($orderAmount);
                $em->persist($order);
            }
        }
    }

    private function updateSettings($os)
    {
        $s = new Entity\Setting();
        $s->setAutomaticOrders($os['auto_order'] == 't');
        $s->setAutomaticReports($os['auto_report'] == 't');
        $s->setMaintenceMode($os['maintenance'] == 't');
        $s->setReportEmail($os['mail_report']);
        $s->setOrderEmail($os['mail_order']);
        $s->setLoginTextBefore('');
        $s->setLoginTextAfter('');
        $s->setMaintainanceMsg('Pls, wait.');

        $this->setIdForObj($s, 1);
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $em->persist($s);
    }

    private function setIdForObj($obj, $oldField)
    {
        $refObject   = new \ReflectionObject( $obj );
        $refProperty = $refObject->getProperty( 'id' );
        $refProperty->setAccessible( true );
        $refProperty->setValue($obj, $oldField);

        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $metadata = $em->getClassMetaData(get_class($obj));
        $metadata->setIdGeneratorType(\Doctrine\Orm\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
    }

    private function handleWarning($message)
    {
        $this->_output->writeln('<error>' . $message . '</error>');
    }

    private function handleError($message)
    {
        $this->_output->writeln('<error>' . $message . '</error>');
        exit;
    }

    private function parseCSV($filename)
    {
        $dir = $this->getPath();
        $filename = $dir . $filename;
        $delimiter = ',';

        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== FALSE)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE)
            {
                if(!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    private function getFromCache($table, $key, $val)
    {
        if (!array_key_exists($table, $this->_cache)) {
            $this->_cache[$table] = $this->parseCSV($table . '.csv');
        }
        
        $items = array();

        foreach ($this->_cache[$table] as $item) {
            if ($item[$key] == $val) {
                $items[] = $item;
            }
        }

        if (empty($items)) {
            return false;
        }

        if ($table == 'disposal_stock') {
                return $items;
        }

        return $items[0];
    }

    private function getPath()
    {
        $path = $this->_input->getOption('path');       
        if (empty($path)) {
            $path = '/tmp/isodb/';
        }

        return $path;
    }
    
    private function convertUserId($id)
    {
        return ++$id;
    }
    
    private function usernameMigrate()
    {
        $header = $oldNameKey = $newNameKey = NULL;
        $row = 1;
        $users = $user_names = array();

        $sheetData = $this->parseODT('Isotopen-DB-Benutzerliste.ods');
        if(empty($sheetData)){
            return false;
        }
        
        $header = $sheetData[$row];
        foreach($header as $key => $item){
            switch ($item) {
                case 'Username':
                    $oldNameKey = $key;
                    break;
                case 'LDAP CN':
                    $newNameKey = $key;
                    break;
                default:
                    break;
            }
        }
        foreach($sheetData as $key => $item){
            if($key == 1) continue;
            $oldName = $item[$oldNameKey];
            $newName = $item[$newNameKey];
            if(empty($oldName) || empty($newName)){
                $this->_output->writeln('<info>Empty username or ldap sn in file in row #'.$key.'.</info>');
                continue;
            }
            if(in_array($newName, $user_names)){
                $this->_output->writeln('<info>Duplicate ldap sn in file in row #'.$key.'.</info>');
                return false;
            }
            $user_names[] = $newName;
        }
        
        $em = $this->getContainer()->get('doctrine')->getManager();
        $em->getConnection()->beginTransaction();
        $userRep = $this->getContainer()->get('doctrine')->getRepository('IsotopeDbBundle:User');
        foreach($sheetData as $key => $item){
            if($key == 1) continue;
            $oldName = $item[$oldNameKey];
            $newName = $item[$newNameKey];
            
            if(empty($oldName) || empty($newName)){
                continue;
            }
            $user = $userRep->findOneByUsername($oldName);
            if(is_null($user)){
                $this->_output->writeln('<info>Row #'.$key.' is not persist in DB. User with old name is absent.</info>');
                continue;
            }

            $user->setUsername($newName);
            $users[$newName] = $user;
        }

        try {
            foreach($users as $key => $item){
                    $em->persist($user);
            }
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
            return $e->getMessage();
        }

        $em->flush();
        $em->getConnection()->commit();
        return 'ok';
    }
    
    private function parseODT($filename)
    {
        $dir = $this->getPath();
        $filename = $dir . $filename;

        if(!file_exists($filename) || !is_readable($filename))
            return FALSE;
        $sheetData = array();
        $sheetname = 'User';

        $reader = PHPExcel_IOFactory::createReader('OOCalc');
        $reader->setLoadSheetsOnly($sheetname);

        try {
            $objPHPExcel = $reader->load($filename);
        } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file "'.pathinfo($filename,PATHINFO_BASENAME).'": '.$e->getMessage());
        }

        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        return $sheetData;
    }
}
