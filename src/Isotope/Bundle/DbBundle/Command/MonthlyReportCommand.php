<?php

namespace Isotope\Bundle\DbBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Isotope\Bundle\DbBundle\Helper as Helper;

class MonthlyReportCommand extends ContainerAwareCommand
{

    private $_input;
    private $_output;

    private $_em = NULL;
    private $_orders = array();

    private $_cache = array();

    /**
     * {@inheritDoc}
     */
    protected function configure()
    {

        $this
            ->setName('mpiz:report:monthly')
            ->setDescription('Send monthly report')
            ->addOption(
                'check-auto',
                null,
                InputOption::VALUE_NONE,
                'Send report if flag \'auto_report\' is set'
            )
        ;

    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->_input = $input;
        $this->_output = $output;
        $this->_dialog = $this->getHelperSet()->get('dialog');

        $rep = $this->getContainer()->get('doctrine')
                ->getRepository('Isotope\Bundle\DbBundle\Entity\Setting');
        $settings = $rep->find(1);
        $automaticReports = $settings->getAutomaticReports();

        $options = $input->getOptions();
        if ($options['check-auto'] == true) {
            if ($automaticReports == false) {
                $this->_output->writeln('<info>Flag is disabled, report is not sent</info>');
                return;
            }
        }

        $reporter = new Helper\ReportHelper($this->getContainer());
        $reporter->sendMonthlyReport();
        
        $this->_output->writeln('<info>done</info>');

    }

}
