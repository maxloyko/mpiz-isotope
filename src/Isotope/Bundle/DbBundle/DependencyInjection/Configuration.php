<?php

namespace Isotope\Bundle\DbBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('isotope_db');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                ->arrayNode('ldap_source_fields')
                    ->children()
                        ->scalarNode('firstname')
                            ->defaultValue('givenname')
                        ->end()
                        ->scalarNode('lastname')
                            ->defaultValue('sn')
                        ->end()
                        ->scalarNode('email')
                            ->defaultValue('mail')
                        ->end()
                        ->scalarNode('phone')
                            ->defaultValue('telephonenumber')
                        ->end()
                        ->scalarNode('roomnumber')
                            ->defaultValue('roomnumber')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
