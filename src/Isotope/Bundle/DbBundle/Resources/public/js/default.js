
$(document).ready(function(){
    $('ul[data-popup="show"] input').on('click', function(){isotopeItemClickPopup(this);});
    $('ul[data-popup="show"] input[type="radio"][checked="checked"]').trigger( "click" );
    showDetail($(this));
    $('select[data-attach="category_amount"]').on('change', function(){showDetail($(this));});
    
    $('.per-page').change(function() {
        window.location = $(this).val();
    });

    // show/hide filters
    var filter_container = $('.filter_container');
    if (filter_container.hasClass('active')) {
        filter_container.show();
    } else {
        filter_container.hide();
    }
    $('.filter_legend').click(function(){
        filter_container.toggle('slow');

        //remove all events
        $( ".filter_legend" ).unbind();

        $(this).click(function(){
            filter_container.toggle('slow');
        });
    });

    //category show/hide releaseLimit depending on internalDisposal
    $('.internalDisposal').click(function () {
        $(".releaseLimit").toggle(this.checked);
    });
    if ($(".internalDisposal:checked").length === 0) {
        $(".releaseLimit").hide();
    }
    
    $('input[data-convert="bq"]').each(function(){
        var name = (($(this).attr('name')).split('[')[1]).split(']');
        var id = (($(this).attr('id')).split('_'))[1];
        var val_bq = parseFloat($(this).val());
        var val = val_bq > 0 ? val_bq/37000 : '';
        var $input = $('<input>',
            {id: id,
            type: "number",
            class: "bq amount_convert",
            name: name,
            'data-convert': "uci",
            value: val });
        var $bq = $('<span>',{class: "physical_quantity"});
        var $uci = $bq.clone();
        var $eq = $bq.clone();
        $(this).hide();
        $(this).after($input);
        $input.after($uci.html('µCi'));
        $uci.after($eq.html('='));
        var $bq_span = $('<span>',
            {class: "bq amount_text",
            html: val > 0 ? val*37000 : 0
        });
        $eq.after($bq_span);
        $bq_span.after($bq.html('Bq'));
    });

    $('input.amount_convert').on('change',function(){
        var uniq = (($(this).parent().children('input[data-convert="bq"]').attr('id')).split('_'))[0];
        var ph_q = $(this).attr('data-convert');
        var val = parseFloat($(this).val());
        var $input = null;
        switch (ph_q) {
            case "bq":
                var id = (($(this).attr('id')).split('_'))[1];
                $input = $(this).parent().children('input[data-convert="uci"]#'+id);
                val /= 37000;
            break;
            case 'uci':
                var id = $(this).attr('id');
                $input = $(this).parent().children('input[data-convert="bq"]#'+uniq+'_'+id);
                val *= 37000;
                $(this).parent().children('span.bq.amount_text').html(val);
                break;
        }
        $input.val(val);
    });
    
    if($('input[type="hidden"].order_status').length) {
        var status = $('input[type="hidden"].order_status').val();
        $('.btn.sonata-action-element').each(function(i,e){
            if(($(e).attr('href')).indexOf('list') !== -1){
                var prev = $(e).attr('href');
                $(e).attr('href',prev+'?filter[status][value]='+status);
            }
        });
    }
    if ($('input[type="hidden"].currentorder_form_design_settings').length) {
        var setting = $('input[type="hidden"].currentorder_form_design_settings').attr('data-setting');
        var arr_set = setting.split(',');
        var actions = $('.well.well-small.form-actions');
        var label = '';
        for (var i = 0; i < arr_set.length; i++) {
            var item = arr_set[i].split(':');
            switch (item[0]) {
                case 'hide':
                    $(actions).find('input[name="' + item[1].trim() + '"]').hide();
                    break;
                case 'active':
                    $(actions).find('input[name="' + item[1].trim() + '"]').addClass('btn-primary');
                    break;
                case 'rename':
                    $(actions).find('input[name="' + item[1].trim() + '"]').val(label);
                    break;
                case 'rename_label':
                    label = item[1].trim();
                    break;
            }
        }
        $('.sonata-actions.btn-group a.btn.sonata-action-element').each(function(i,e) {
            if(($(e).attr('href')).indexOf('currentorder/create') !== -1){
                $(e).hide();
            }
        });
    }
});

function isotopeItemClickPopup(elem){
    var id = $(elem).val();
        $('ul[data-popup="show"] li').each(function(){
            $(this).removeClass('selected_li');
        });

        $(elem).parent().parent().toggleClass('selected_li');

        if (!$(".isotope_detail")[0]){
            $('ul[data-popup="show"]').after('<div class="isotope_detail"><table></table></div>');
        }
        $('.isotope_detail').hide();
        $.ajax({
            type : "GET",
            url : '/admin/isotope/db/isotope/{id}/show',
            data: {id: id},
            dataType : "html",
            success: function(responseText) {
                var table = $(responseText).html();
                $('.isotope_detail table').html(table);
                
                $('.isotope_detail .sonata-ba-view-title').html('<td colspan=2>Details:</td>');
                var rows = $('.isotope_detail .sonata-ba-view-container');
                $(rows[12]).remove();
                $(rows[11]).remove();
                $(rows[6]).remove();
                $(rows[5]).remove();
                $(rows[3]).remove();
                $(rows[1]).remove();
                $(rows[0]).remove();
                $(rows).each(function(i,e){
//                    var td_value = $(e).children('td');
                    if($(e).children('td').text() === ''){
                        $(rows[i]).hide();
                    }
                });
                $('.isotope_detail').show();
                $('.isotope_detail').after('<div class="clr"></div>');
            }
        });
}

function showDetail(elem)
{
    if($('select[data-attach="category_amount"]').length === 0) return;
    var id = 0;
    if($(elem).val()){
        id = $(elem).val();
    } else {
        id = $(elem).find('option:first').val();

    }
    if (!$(".isotope_detail")[0]){
        $('select[data-attach="category_amount"]').parent().
                append('<div class="isotope_detail"><div><p>Details:</p><p>Aktueller Bestand (µCi): <span></span></p></div></div>');
    }
    $('.isotope_detail span').hide();
    $.getJSON("/admin/isotope/db/internaldisposal/category_disposal/"+id, null, function(json){
        $('.isotope_detail span').html(json.amount);
        $('.isotope_detail span').show();
    });
}
