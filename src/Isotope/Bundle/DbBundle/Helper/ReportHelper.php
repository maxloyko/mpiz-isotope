<?php

namespace Isotope\Bundle\DbBundle\Helper;

use Isotope\Bundle\DbBundle\Entity as Entity;

class ReportHelper
{
    private $_container;

    public function __construct($container) {
        $this->_container = $container;
    }

    public function sendWeeklyReport()
    {

        $em = $this->_container->get('doctrine')->getEntityManager();
        $em->getConnection()->beginTransaction();

        $orders = $em->getRepository('IsotopeDbBundle:Order')->filterByLastWeek('order');
        $isotops = array();

        if (count($orders)  == 0) {
            return true;
        }

        try { 
            $orders_stock = array();
            $isotope = array('entity' => NULL, 'sum' => 0);
            foreach($orders as $i => $order) {
                if ($order->getIsotope() !== $isotope['entity']) {
                    if ($isotope['sum'] > 0 && $dummy_order = $this->addDummyOrder($isotope, $em) ) {
                        $orders_stock[] = $dummy_order;
                        $isotope['sum'] += $dummy_order->getAmount();
                    }
                    if (!is_null($isotope['entity'])) {
                        $isotops[$isotope['entity']->getId()] = $isotope['sum']/37000;
                    }
                    $isotope['sum'] = 0;
                    $isotope['entity'] = $order->getIsotope();

                }
                $isotope['sum'] += $order->getAmount();

                $order->setStatus('stock');
                $orders_stock[] = $order;

                $em->persist($order);
            }

            if ($isotope['sum'] > 0 && $dummy_order = $this->addDummyOrder($isotope, $em) ) {
                $orders_stock[] = $dummy_order;
                $isotope['sum'] += $dummy_order->getAmount();
            }
            if (!is_null($isotope['entity'])) {
                $isotops[$isotope['entity']->getId()] = $isotope['sum']/37000;
            }
        } catch(\Exception $e) {
            $em->getConnection()->rollback();
            $em->close();
        }
        $em->flush();
        $em->getConnection()->commit();

        //send mail
        $week = date('W');

        $date = new \DateTime('now');
        $filename = "weekly_orders_report_$week" . '_' . (!is_null($date) ? $date->format('Ymd_His') : '');

        $reportName = __DIR__.'/../../../../../reports/'.$filename . '.txt';
        if (file_exists($reportName)) {
            $reportName = __DIR__ . '/../../../../../reports/' . $filename . '_' . (!is_null($date) ? $date->format('u') : '') . '.txt';
        }
        $messageBody = $this->_container->get('templating')->render(
            'IsotopeDbBundle:Admin:weekly_email.txt.twig', array(
                'orders_stock' => $orders_stock,
                'isotops'      => $isotops,
                'week'         => $week,
                )
        );
        file_put_contents($reportName, $messageBody);

        $settings = $this->_container->get('doctrine')
            ->getRepository('IsotopeDbBundle:Setting')->find(1);
        $orderEmail = explode(',', $settings->getOrderEmail());

        $message = \Swift_Message::newInstance()
            ->setSubject('Weekly Orders')
            ->setFrom($this->_container->getParameter('reports_sender'))
            ->setTo(!empty($orderEmail) ? $orderEmail : '')
            ->setBody($messageBody)
        ;
        $this->_container->get('mailer')->send($message);

        return true;
    }
    
    public function getBalance(\DateTime $date = null)
    {
        $em = $this->_container->get('doctrine')->getManager();
        $categories = $em->getRepository('IsotopeDbBundle:Category')->findAll();
        $table = array();

        foreach ($categories as $category) {
            $table[$category->getId()] = array(
                'category'          => $category->getName(),
                'initial_stock'     => 0,
                'initial_stock_ci'     => 0,
                'purchased'         => 0,
                'purchased_ci'         => 0,
                'disposed'          => 0,
                'disposed_ci'          => 0,
                'difference'        => 0,
                'difference_ci'        => 0,
                'end_stock'         => 0,
                'end_stock_ci'         => 0,
            );
        }

        if(null == $date)
        {
            $date = new \DateTime('today');
        }

        $start_month =  $date->modify('first day of this month')->format('Y-m-d');

        $sql = "SELECT sum(`change`) as `amount`, category FROM logger WHERE date < :date  GROUP BY category";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $start_month);
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            $table[$result['category']]['initial_stock'] = $result['amount'];
            $table[$result['category']]['initial_stock_ci'] = $result['amount'] / 37000;
            $table[$result['category']]['end_stock'] = $result['amount'];
            $table[$result['category']]['end_stock_ci'] = $result['amount'] / 37000;
        }

        $sql = "select category, sum(`change`) as amount, comment from logger where DATE_FORMAT(date, '%Y-%m') = :date  group by category, comment";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y-m'));
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            if ($result['amount'] > 0) {
                $table[$result['category']]['purchased'] += $result['amount'];
                $table[$result['category']]['purchased_ci'] += $result['amount'] / 37000;
            } else {
                $table[$result['category']]['disposed'] -= $result['amount'];
                $table[$result['category']]['disposed_ci'] -= $result['amount'] / 37000;
            }
            $table[$result['category']]['difference'] += $result['amount'];
            $table[$result['category']]['difference_ci'] += $result['amount'] / 37000;
            $table[$result['category']]['end_stock'] = $table[$result['category']]['initial_stock']
                                                     + $table[$result['category']]['difference'];
            $table[$result['category']]['end_stock_ci'] = $table[$result['category']]['initial_stock_ci']
                                                     + $table[$result['category']]['difference_ci'];
        }

        return !empty($table) ? $table : array();
    }
    public function getBalanceYear(\DateTime $date = null)
    {
        $em = $this->_container->get('doctrine')->getManager();
        $categories = $em->getRepository('IsotopeDbBundle:Category')->findAll();
        $table = array();

        foreach ($categories as $category) {
            $table[$category->getId()] = array(
                'category'          => $category->getName(),
                'initial_stock'     => 0,
                'initial_stock_ci'     => 0,
                'purchased'         => 0,
                'purchased_ci'         => 0,
                'disposed'          => 0,
                'disposed_ci'          => 0,
                'difference'        => 0,
                'difference_ci'        => 0,
                'end_stock'         => 0,
                'end_stock_ci'         => 0,
            );
        }

        if(null == $date)
        {
            $date = new \DateTime('today');
        }

        $start_month =  $date->modify('first day of this year')->format('Y-m-d');

        $sql = "SELECT sum(`change`) as `amount`, category FROM logger WHERE date < :date  GROUP BY category";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $start_month);
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            $table[$result['category']]['initial_stock'] = $result['amount'];
            $table[$result['category']]['initial_stock_ci'] = $result['amount'] / 37000;
            $table[$result['category']]['end_stock'] = $result['amount'];
            $table[$result['category']]['end_stock_ci'] = $result['amount'] / 37000;
        }

        $sql = "select category, sum(`change`) as amount, comment from logger where DATE_FORMAT(date, '%Y') = :date  group by category, comment";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y'));
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            if ($result['amount'] > 0) {
                $table[$result['category']]['purchased'] += $result['amount'];
                $table[$result['category']]['purchased_ci'] += $result['amount'] / 37000;
            } else {
                $table[$result['category']]['disposed'] -= $result['amount'];
                $table[$result['category']]['disposed_ci'] -= $result['amount'] / 37000;
            }
            $table[$result['category']]['difference'] += $result['amount'];
            $table[$result['category']]['difference_ci'] += $result['amount'] / 37000;
            $table[$result['category']]['end_stock'] = $table[$result['category']]['initial_stock']
                                                     + $table[$result['category']]['difference'];
            $table[$result['category']]['end_stock_ci'] = $table[$result['category']]['initial_stock_ci']
                                                     + $table[$result['category']]['difference_ci'];
        }

        return !empty($table) ? $table : array();
    }

    public function getDetailLogForMonthlyReport(\DateTime $date, $isYearly = false)
    {
        $em = $this->_container->get('doctrine')->getManager();

        $isYearly ? $dateFormat = 'Y' : $dateFormat = 'Y-m';
        $isYearly ? $dbDateFormat = '%Y' : $dbDateFormat = '%Y-%m';
        $sql = "select c.name as category, l.date, sum(l.change) as `amount` from logger as l LEFT JOIN category as c on l.category = c.id  where DATE_FORMAT(date, '" . $dbDateFormat . "') = :date and l.change > 0 group by c.name, l.date";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format($dateFormat));
        $stmt->execute();

        return $result = $stmt->fetchAll();
    }

    public function getAnnualBalance(\DateTime $date = null)
    {
        $em = $this->_container->get('doctrine')->getManager();
        $categories = $em->getRepository('IsotopeDbBundle:Category')->findAll();
        $table = array();

        foreach ($categories as $category) {
            $table[$category->getId()] = array(
                'category'          => $category->getName(),
                'purchased'         => 0,
                'purchased_ci'         => 0,
                'disposed'          => 0,
                'disposed_ci'          => 0
            );
        }

        if (null == $date)
        {
            $date = new \DateTime('today');
        }


        $sql = "SELECT sum(`change`) as `amount`, category FROM logger WHERE DATE_FORMAT(date, '%Y') = :date AND discr = 'stock' GROUP BY category";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y'));
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            $table[$result['category']]['purchased'] = $result['amount'];
            $table[$result['category']]['purchased_ci'] = $result['amount'] / 37000;
        }

        $sql = "select category, sum(`change`) as amount, comment from logger where DATE_FORMAT(date, '%Y') = :date AND (discr = 'internal_disposal' OR discr = 'external_disposal') group by category ORDER BY date";

        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y'));
        $stmt->execute();

        while ($result = $stmt->fetch()) {
            $table[$result['category']]['disposed'] = $result['amount'];
            $table[$result['category']]['disposed_ci'] = $result['amount'] / 37000;

        }

        return !empty($table) ? $table : array();
    }

    public function sendMonthlyReport()
    {
        $date = new \DateTime('first day of last month');
        $table = $this->getBalance($date);
        $settingsMonthRep = $this->_container->get('doctrine')
            ->getRepository('IsotopeDbBundle:MonthlyReportSetting')->find(1);
        $filename = $this->generateBalancePdf($table, $date, $settingsMonthRep);

        $settings = $this->_container->get('doctrine')
                    ->getRepository('IsotopeDbBundle:Setting')->find(1);
        $reportEmail = explode(',', $settings->getReportEmail());
        $mailData = array(
            'subject' => 'Balance Report ' . $date->format('Y') . ' ' . $date->format('m'),
            'to'      => !empty($reportEmail) ? $reportEmail : '',
            'body'    => 'This is a automatic generated monthly report. ' . "\n"
            . 'Please do not response to this e-mail.' . "\n",
            'attach'  => $filename,
        );
        $this->sendReport($mailData);
    }
    
    public function generateBalancePdf($table, \DateTime $date, $settingsMonthRep, $isYearly = false)
    {
        $detail_logs = $this->getDetailLogForMonthlyReport($date, $isYearly);

        $filename = 'balance_report';
        if ($isYearly) {
            $filename = 'balance_yearly_report';
        }
        $filename .= (!is_null($date) ? '_'.$date->format('Y').'_'.$date->format('m') : '').
            '.pdf';
        $reportName = __DIR__.'/../../../../../reports/'.$filename;
        $dateNow = new \DateTime('today');

            if(file_exists($reportName))
            {
                unlink($reportName);
            }

        if(!file_exists($reportName) ||  (($date->format('Y') == $dateNow->format('Y')) &&
                ($date->format('m') == $dateNow->format('m'))))
        {
            if(file_exists($reportName))
            {
                unlink($reportName);
            }


            $title = 'Report '.(!is_null($date) ? $date->format('Y').' '.$date->format('m') : '');
            if ($isYearly) {
                $title = 'Yearly balance for ' . $date->format('Y');
                $settingsMonthRep->setContent('Automatisch generierter Bericht für das ganze Jahr ' . $date->format('Y') . ', welcher nicht zum versenden gedacht ist.');

            }

            $this->_container->get('knp_snappy.pdf')->generateFromHtml(
                $this->_container->get('templating')->render(
                    'IsotopeDbBundle:Pdf:sendBalanceReport.html.twig',
                    array(
                        'title' => $title,
                        'reports'  => $table,
                        'settings' => $settingsMonthRep,
                        'date'     => $dateNow,
                        'logs'     => $detail_logs,
                    )
                ),$reportName,
                array(
                    'title' => $title, 
                    'footer-html' => $this->_container->get('templating')->render(
                        'IsotopeDbBundle:Pdf:sendBalanceReportFooter.html.twig'),
                    'no-footer-line' => false,
                    'footer-spacing' => 1,
                    'margin-left' => 20,
                    'margin-top' => 17,
                    'margin-right' => 10,
                    'margin-bottom' => 43,)
                
            );
        }
        return file_exists($reportName) ? $reportName : null;
    }

    public function generateDisposalPdf($internal_disposal, $user)
    {
        $filename = 'disposal_'.$internal_disposal->getId().'.pdf';
        $reportName = __DIR__.'/../../../../../reports/'.$filename;

        if(!file_exists($reportName))
        {
            $this->_container->get('knp_snappy.pdf')->generateFromHtml(
                $this->_container->get('templating')->render(
                    'IsotopeDbBundle:Pdf:internal_disposal.html.twig', array(
                    'internal_disposal'  => $internal_disposal,
                     'user' => $user
                    )
                ),
                $reportName
            );
        }
        return file_exists($reportName) ? $reportName : null;
    }

    public function sendReport($data)
    {
        if(!empty($data) && !empty($data['to']))
        {
            $data['to'] = !is_array($data['to']) ? explode(',', $data['to']) : $data['to'];
            $mailer = $this->_container->get('mailer');
            $message = \Swift_Message::newInstance()
                ->setSubject($data['subject'])
                ->setFrom($this->_container->getParameter('reports_sender'))
                ->setTo($data['to'])
                ->setBody($data['body'])
                ;
            if(!is_null($data['attach']) && file_exists($data['attach']))
            {
                $message->attach(\Swift_Attachment::fromPath($data['attach']));
            }
            $mailer->send($message);
            return true;
        } else {
            return false;
        }
    }

    private function addDummyOrder($isotope, $em) {

        if ($isotope['sum'] % $isotope['entity']->getRounding() == 0) {
            return false;
        }

        $dummyOrder = new Entity\Order();
        $dummyUser = $this->_container->get('doctrine')->getRepository('IsotopeDbBundle:User')->findOneByUsername('DUMMY');
        $dummyOrder->setUser($dummyUser);
        $dummyOrder->setDate(new \DateTime('today'));
        $dummyOrder->setIsotope($isotope['entity']);
        $dummyOrder->setStatus('stock');
        $dummyOrder->setCostCenter('200');
        $dummyOrder->setAmount($isotope['entity']->getRounding() - $isotope['sum'] % $isotope['entity']->getRounding());
        $dummyOrder->setVisible(true);

        $em->persist($dummyOrder);

        return $dummyOrder;
    }

    public function getAnnualCorrection(\DateTime $date = null)
    {
        $em = $this->_container->get('doctrine')->getManager();
        $categories = $em->getRepository('IsotopeDbBundle:Category')->findAll();
        $table = array();

        foreach ($categories as $category) {
            $table[$category->getName()] = $category->getId();
        }
        if (null == $date)
        {
            $date = new \DateTime('today');
        }

        $em = $this->_container->get('doctrine')->getManager();

        $sql = "select category, `change` as amount, date from logger where DATE_FORMAT(date, '%Y') = :date AND (discr = 'internal_disposal' OR comment = 'correction') ORDER BY date";
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->bindValue('date', $date->format('Y'));
        $stmt->execute();


        $results = $stmt->fetchAll();
        $t = array();
        foreach($results as $r) {
            $t[] = array('category' => array_search($r['category'], $table),
                        'amount' => $r['amount'],
                        'amount_ci' => $r['amount'] / 37000,
                        'date' => $r['date']
            );
        }
        return !empty($t) ? $t : array();

    }
}
?>
