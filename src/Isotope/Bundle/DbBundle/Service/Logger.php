<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/14/14
 * Time: 4:22 PM
 */

namespace Isotope\Bundle\DbBundle\Service;

use Symfony\Component\Security\Core\SecurityContext;

use Doctrine\ORM\EntityManager;

class Logger {

    private $em;
    /**
     * @var SecurityContext
     */
    protected $context;

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }

    public function __construct(SecurityContext $context)
    {
        $this->context = $context;
    }

    public function writeLog ($object, $comment, $change)
    {

        $isotope = NULL;
        $object_id = $object->getId();

        switch (substr(strrchr(get_class($object), "\\"), 1)) {
            case 'Order':
                $discr = 'stock';
                $isotope = $object->getIsotope()->getId();
                $category = $object->getIsotope()->getCategory()->getId();
                if ($comment == 'correction') {
                    $change = -$object->getAmount();
                } else {
                    $change = $object->getAmount();
                }
                if ($object->getUser() != null) {
                    $user = $object->getUser()->getId();
                } else {
                    $user = 0;
                }
                $date = $object->getStockDate();
                break;
            case  'InternalDisposal':
                $discr = 'internal_disposal';
                if ($comment == 'correction') {
                    $change = $object->getAmount();
                } else {
                    $change = -$object->getAmount();
                }
                $date = $object->getActualDisposal();
                break;
            case  'ExternalDisposal':
                $discr = 'external_disposal';
                $change = -$object->getAmount();
                $date = $object->getDate();
                break;
        }

        if (!isset($user) && !isset($category))
        {
            $category = $object->getCategory()->getId();
            if ($this->context->getToken()) {
                $user = $this->context->getToken()->getUser()->getId();
            } else {
                $user = 0;
            }
        }
        $result = $date->format('Y-m-d H:i:s');

        $sql = "INSERT INTO logger (comment, category, date, user, `change`, discr, isotope, object_id)
                    VALUES (:comment, :category, :date, :user, :change, :discr, :isotope, :object_id )";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('comment', $comment);
        $stmt->bindValue('category', $category);
        $stmt->bindValue('date', $result );
        $stmt->bindValue('user', $user);
        $stmt->bindValue('change', $change);
        $stmt->bindValue('discr',$discr);
        $stmt->bindValue('isotope', $isotope);
        $stmt->bindValue('object_id', $object_id);
        $stmt->execute();

    }

    public function removeLog ($object_id)
    {
        $sql = "DELETE FROM logger WHERE object_id = :object_id";
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->bindValue('object_id', $object_id);
        $stmt->execute();
    }



} 
