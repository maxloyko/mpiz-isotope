<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/23/14
 * Time: 3:43 PM
 */

namespace Isotope\Bundle\DbBundle\Service;

use Isotope\Bundle\DbBundle\Entity\MonthlyReportSetting as MonthlyReportSetting;
use Doctrine\ORM\EntityManager;


class ReportSetting {
    /**
     * @var EntityManager
     */
    private $em;

    private $report_setting;

    public function getMonthlyReportSetting()
    {
        $this->report_setting = $this->em->getRepository('IsotopeDbBundle:MonthlyReportSetting')->find(1);

        if (!$this->report_setting) {
            $this->report_setting = new MonthlyReportSetting();
        }

        return $this->report_setting;
    }

    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
    }
} 