<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 2/3/14
 * Time: 3:14 PM
 */

namespace Isotope\Bundle\DbBundle\Twig;


class IsotopeDbExtension extends \Twig_Extension
{
    public function getName()
    {
        return 'path_to_image';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'path_to_image' => new \Twig_Function_Method($this, 'path')
        );
    }

    public function getFilters()
    {
        return array(
            'science_number_format' => new \Twig_Filter_Method($this, 'scienceNumberFormat'),
            'german_number_format' => new \Twig_Filter_Method($this, 'germanNumberFormat')
        );
    }

    public function path ($name_img)
    {
        return realpath(__DIR__ . '/../Resources/public/images/' .$name_img);
    }

    public function scienceNumberFormat($number, $digits = null, $german = false) {

        if (!is_numeric($number) || abs($number) < pow(10, 5)) {
            return $number;
        }

        $format = ($digits === null) ? '%E' : "%.{$digits}E";
        $pattern = '/(0+E)/i';

        $number = preg_replace($pattern, 'E', sprintf($format, $number));

        if ($german) {
            $number = str_replace('.', ',', $number); 
        }

        return $number;
    }

    private function ownNumberFormat($number, $digits = null, $dec_point = '.', $thousands_sep = '') {

        if ($digits !== null) {
            return number_format($number, $digits, $dec_point, $thousands_sep);
        }

        $tmp = explode('.', $number); 
        $out = number_format($tmp[0], 0, $dec_point, $thousands_sep); 
        if (isset($tmp[1])) $out .= $dec_point.$tmp[1]; 

        return $out; 
    }

    public function germanNumberFormat($number, $digits = null) {
        return $this->ownNumberFormat($number, $digits, ',', '.');
    }
} 
