<?php

namespace Isotope\Bundle\DbBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Cookie;

class MaintenanceListener
{
    private $container;
    private $securityContext;

    public function __construct(ContainerInterface $container, SecurityContext $securityContext)
    {
        $this->container = $container;
        $this->securityContext = $securityContext;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $settings = $em->getRepository('IsotopeDbBundle:Setting')->find(1);
        $maintainanceMode = $settings->getMaintenceMode();
        $maintainanceMsg = $settings->getMaintainanceMsg();
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        if ($maintainanceMode && !(strcmp($routeName,'isotope_login') == 0) && !(strpos($routeName,'_') === 0)) {
            if(!$this->securityContext->isGranted('ROLE_ISOTOP_ADMIN')){
                $url = $this->container->get('router')->generate('logout');
                $response = new RedirectResponse($url);
                $response->headers->setCookie(new Cookie('maintain', 'true', time() + (3600 * 48)));
                $response->headers->setCookie(new Cookie('maintain_user',
                        $this->securityContext->getToken()->getUsername(), time() + (3600 * 48)));
                $event->setResponse($response);
                $event->stopPropagation();
            }
        }
    }
}

