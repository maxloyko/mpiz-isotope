<?php

namespace Isotope\Bundle\DbBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Cookie;

class UsersListener
{
    private $container;
    private $securityContext;

    public function __construct(ContainerInterface $container, SecurityContext $securityContext)
    {
        $this->container = $container;
        $this->securityContext = $securityContext;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $this->container->get('request');
        $routeName = $request->get('_route');
        $session = $this->container->get('session');
        $whitelist = array(
            'isotope_db_homepage',
            'isotope_login','logout',
            'admin_isotope_db_currentorder_create',
            'admin_isotope_db_currentorder_list','admin_isotope_db_currentorder_edit',
            'admin_isotope_db_currentorder_batch','admin_isotope_db_currentorder_delete',
            'admin_isotope_db_currentorder_show','admin_isotope_db_currentorder_export',
            'login_check','admin_isotope_db_isotope_show',
            'sonata_admin_redirect','sonata_admin_retrieve_form_element',
            'sonata_admin_short_object_information','sonata_admin_set_object_field_value');

        if (!in_array($routeName, $whitelist) && !(strpos($routeName,'_') === 0)) {
            if(!$this->securityContext->isGranted('ROLE_ISOTOP_ADMIN') && !$this->securityContext->isGranted('ROLE_ISOTOP_USER')){
                $route = $this->container->get('router')->generate($whitelist[2]);
                $session->getFlashBag()->set('sonata_flash_error', 'Access Denied. You do not have permission to access this page.');
                $event->setResponse(new RedirectResponse($route));
                $event->stopPropagation();
            } else if(!$this->securityContext->isGranted('ROLE_ISOTOP_ADMIN') && $this->securityContext->isGranted('ROLE_ISOTOP_USER')){
                $route = $this->container->get('router')->generate($whitelist[3]);
                $session->getFlashBag()->set('sonata_flash_error', 'Access Denied. You do not have permission to access this page.');
                $event->setResponse(new RedirectResponse($route));
                $event->stopPropagation();
            }
        }
        $whitelist = array(
            'isotope_db_homepage',
            'isotope_login','login',
            'login_check','logout',
            'sonata_admin_redirect','sonata_admin_retrieve_form_element',
            'sonata_admin_short_object_information','sonata_admin_set_object_field_value');
        if (!in_array($routeName, $whitelist) && !(strpos($routeName,'_') === 0)) {
            if(!$this->securityContext->isGranted('ROLE_ISOTOP_ADMIN') &&
                !$this->securityContext->isGranted('ROLE_ISOTOP_USER')
                ){

                $url = $this->container->get('router')->generate('logout');
                $response = new RedirectResponse($url);
                $response->headers->setCookie(new Cookie('not_user_role', 'true', time() + (3600 * 48)));
                $event->setResponse($response);
                $event->stopPropagation();
            }
        }
    }
}

