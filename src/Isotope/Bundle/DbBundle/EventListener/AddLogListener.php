<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/15/14
 * Time: 3:18 PM
 */

namespace Isotope\Bundle\DbBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

use Isotope\Bundle\DbBundle\Entity\Order;
use Isotope\Bundle\DbBundle\Entity\ExternalDisposal;
use Isotope\Bundle\DbBundle\Entity\InternalDisposal;


use Symfony\Component\DependencyInjection\ContainerInterface;

class AddLogListener
{
    private  $activity_logger;

    private  $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

    }

    private function getActivityLogger()
    {
        if (!isset($this->activity_logger))
            $this->activity_logger = $this->container->get('activity_logger');
        return $this->activity_logger;
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Order) {
            if ($args->hasChangedField('status') && $args->getNewValue('status') == 'stock') {
                $this->getActivityLogger()->writeLog($entity, 'purchase', true);
            }
            if ($args->hasChangedField('status') && $args->getNewValue('status') == 'deleted (stock)') {
                $date_now = new \DateTime('now');
                $date_month =  $date_now->format('Y-m');
                if ($entity->getStockDate()->format('Y-m') ==  $date_month) {
                    $object_id = $entity->getId();
                    $this->getActivityLogger()->removeLog($object_id);
                } else {
                    $category = $entity->getIsotope()->getCategory();
                    $amount = $entity->getAmount();
                    $internal_disposal = new InternalDisposal();
                    $internal_disposal->setAmount(-$amount);
                    $internal_disposal->setActualDisposal($entity->getStockDate());
                    $internal_disposal->setCategory($category);

                    $this->getActivityLogger()->writeLog($internal_disposal, 'correction', false);
                }
            }
        }
        if ($entity instanceof InternalDisposal) {
            if ($args->hasChangedField('actualDisposal') && !is_null($args->getNewValue('actualDisposal'))) {
                $this->getActivityLogger()->writeLog($entity, 'approved internal disposal', false);
            }
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Order) {
            if ($entity->getStatus() == 'stock') {
                $this->getActivityLogger()->writeLog($entity, 'purchase', true);
            }

            if ($entity->getStatus() == 'deleted (stock)') {
                $this->getActivityLogger()->writeLog($entity, 'correction', false);
            }
        }

        if ($entity instanceof InternalDisposal && !is_null($entity->getActualDisposal())) {
            $this->getActivityLogger()->writeLog($entity, 'approved internal disposal', false);
        }

        if ($entity instanceof ExternalDisposal) {
            $this->getActivityLogger()->writeLog($entity, 'external disposal', false);
        }
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof ExternalDisposal) {
            $this->getActivityLogger()->writeLog($entity, 'correction', true);
        }
    }
}
