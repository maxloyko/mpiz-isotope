<?php

namespace Isotope\Bundle\DbBundle\Type;

class StatusEnumType extends EnumType
{
    protected $name = 'status';
    protected $values = array('order','stock','deleted (order)','deleted (stock)');
}
