<?php

namespace Isotope\Bundle\DbBundle\Type;

class DeliveryEnumType extends EnumType
{
    protected $name = 'delivery';
    protected $values = array('internal','external');
}