<?php

namespace Isotope\Bundle\DbBundle\Type;

class LoggerEnumType extends EnumType
{
    protected $name = 'logger';
    protected $values = array('purchase', 'correction', 'approved internal disposal', 'external disposal');
}