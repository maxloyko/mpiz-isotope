<?php
// src/Isotope/Bundle/DbBundle/Repository/OrderRepository.php
namespace Isotope\Bundle\DbBundle\Repository;

use Isotope\DbBundle\Entity;
use Doctrine\ORM as ORM;
use Doctrine\ORM\EntityRepository;

class OrderRepository extends EntityRepository {

    public function filterByLastWeek($status = NULL) {

        $mn = new \DateTime(date('d.m.Y'));

        $query = $this->createQueryBuilder('o')
            ->andWhere('o.date <= :today')
            ->setParameter('today', $mn->format('Y-m-d'));

        if (!empty($status)) {
            $query  ->andWhere('o.status = :status')
                    ->setParameter('status', $status);
        }
        
        $query->orderBy('o.isotope', 'ASC');

        return $query->getQuery()->getResult();
    }

    public function filterForDispose($category) {

        $query = $this->createQueryBuilder('o')
            ->innerJoin('o.isotope', 'i')
            ->andWhere('i.category = :category')
            ->andWhere('o.visible = :visible')
            ->andWhere('o.status = :status')
            ->setParameter('category', $category)
            ->setParameter('status', 'stock')
            ->setParameter('visible', true)
            ->orderBy('o.date','ASC');

        return $query->getQuery()->getResult();
    }

    public function filterForUndispose($category) {

        $query = $this->createQueryBuilder('o')
            ->innerJoin('o.isotope', 'i')
            ->andWhere('i.category = :category')
            ->andWhere('o.status = :status')
            ->setParameter('category', $category)
            ->setParameter('status', 'stock')
            ->orderBy('o.date','DESC');

        return $query->getQuery()->getResult();
    }
}

?>
