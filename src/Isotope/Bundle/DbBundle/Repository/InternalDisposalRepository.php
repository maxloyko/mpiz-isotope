<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 2/6/14
 * Time: 12:56 PM
 */

namespace Isotope\Bundle\DbBundle\Repository;

use Isotope\DbBundle\Entity;
use Doctrine\ORM as ORM;
use Doctrine\ORM\EntityRepository;


class InternalDisposalRepository extends EntityRepository
{
    public function filterByAnnual(\DateTime $date) {

        $query = $this->createQueryBuilder('d')
            ->andWhere("DATE_FORMAT(d.actualDisposal, '%Y')= :date")
            ->setParameter('date', $date->format('Y'));

        return $query->getQuery()->getResult();
    }
}
