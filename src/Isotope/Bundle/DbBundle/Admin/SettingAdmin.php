<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/13/14
 * Time: 1:53 PM
 */

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;


class SettingAdmin  extends Admin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Einstellungen')
                ->add('automatic_orders', 'checkbox', array('required' => false))
                ->add('automatic_reports', 'checkbox', array('required' => false))
                ->add('maintence_mode', 'checkbox', array('required' => false))
                ->add('report_email','text', array('required' => false))
                ->add('order_email','text', array('required' => false))
                ->add('login_text_before','textarea', array('required' => false))
                ->add('login_text_after','textarea', array('required' => false))
                ->add('maintainance_msg','textarea', array('required' => false));
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // Only `edit` route will be active
        $collection
            ->clearExcept(array('edit'))
            ->add('admin_weekly_report',
                'admin_weekly_report',
                array('_controller' => 'IsotopeDbBundle:Admin:sendWeeklyReport')
                //array('id' => '\d+')
            )
        ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'edit':
                return 'IsotopeDbBundle::Admin/settings-edit.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }

} 
