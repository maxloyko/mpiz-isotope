<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route as Route; 
use Sonata\AdminBundle\Validator\ErrorElement;


use Doctrine\ORM\EntityRepository as Repository;
class InternalDisposalAdmin extends Admin
{
//    public function prePersist($intrnalDisposal)
//    {
//        $intrnalDisposal->disposalDate = DateTime::createFromFormat('c',  time());//date('c',  time());
//    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('category')
            ->add('fillingStart')
            ->add('fillingEnd')
            ->add('date', null, array('label' => 'Datum der Einlagerung'))
            ->add('actualDisposal')
            ->add('barrelCategory')
            ->add('barrelNumber')
            ->add('barrelPackageWeight')
            ->add('totalWeight')
            ->add('amount')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('barrelLabel')
            ->add('amount', null, array(
                'sortable' => true,
                'template' => 'IsotopeDbBundle:Admin:list_amount.html.twig',
                ))
            ->add('category')
            ->add('comment')
            ->add('date', 'datetime', array('label' => 'Datum der Einlagerung','format' => 'd.m.Y'))
            ->add('actualDisposal', 'datetime', array('format' => 'd.m.Y'))
            ->add('calculatedDisposalDate', 'datetime', array('format' => 'd.m.Y'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'pdf'       => array('template' => 'IsotopeDbBundle:Admin:list__action_pdf.html.twig'),
                    'mail'       => array('template' => 'IsotopeDbBundle:Admin:list__action_mail.html.twig'),
                    'approve'   => array('template' => 'IsotopeDbBundle:Admin:approve_internal_disposal_action.html.twig'),
                    'delete'    => array('template' => 'IsotopeDbBundle:Admin:delete_internal_disposal_action.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Interne Entsorgung')
            ->add('category',null,array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Category',
                'required' => true,
                'attr' => array('data-attach' => 'category_amount')
                ))
            ->add('fillingStart', null, array('required' => true))
            ->add('fillingEnd', null, array('required' => true))
            ->add('date', 'date', array('label' => 'Datum der Einlagerung','required' => true))

            ->add('barrelCategory',null,array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Barrel',
                'property' => 'selectLabel',
                'query_builder' => function (Repository $repository) {
                    return $repository->createQueryBuilder('e')
                            ->where('e.visible = true')
                            ->orderBy('e.barrelsCategory', 'ASC')
                            ->addOrderBy('e.id', 'ASC');
                },
                'required' => true))
            ->add('barrelNumber', null, array('required' => true))
            ->add('barrelPackageWeight', null, array('required' => true))
            ->add('totalWeight', null, array('required' => true))
            ->add('amount', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => true))
            ->add('comment', null, array('required' => false))
        ;
    }

    // Remove actions
    public function getBatchActions()
    {
        return null;
    }

    protected function configureRoutes(Route\RouteCollection $collection)
    {
        $collection->add('approve', $this->getRouterIdParameter() . '/approve');
        $collection->remove('edit');
        $collection->add('category_disposal', 'category_disposal/'.$this->getRouterIdParameter());
        $collection->add('send_disposal', 'send_disposal/'.$this->getRouterIdParameter());
    }

    public function getNewInstance()
    {
        $disposal = parent::getNewInstance();
        $disposal->setFillingStart( new \DateTime( 'today' ) );
        $disposal->setFillingEnd( new \DateTime( 'today' ) );
        $disposal->setDate( new \DateTime( 'today' ) );

        return $disposal;
    }
    
    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('category')
                ->assertNotBlank()
            ->end()
            ->with('fillingStart')
                ->assertNotBlank()
                ->assertDate()
            ->end()
            ->with('fillingEnd')
                ->assertNotBlank()
                ->assertDate()
            ->end()
            ->with('date')
                ->assertNotBlank()
                ->assertDate()
            ->end()
            ->with('barrelCategory')
                ->assertNotBlank()
            ->end()
            ->with('barrelNumber')
                ->assertNotBlank()
            ->end()
            ->with('barrelPackageWeight')
                ->assertNotBlank()
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
            ->with('totalWeight')
                ->assertNotBlank()
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
            ->with('amount')
                ->assertNotBlank()
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
        ;

        if ($isotope->getFillingStart()->getTimestamp() > $isotope->getFillingEnd()->getTimestamp()) {
            $errorElement->with('fillingStart')->addViolation('Start date should go before the end date.')->end();
            $errorElement->with('fillingEnd')->addViolation('Start date should go before the end date.')->end();
        }
    }
    /**
     * Default Datagrid values
     *
     * @return array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
                '_sort_order' => 'DESC', // sort direction
                '_sort_by' => 'id' // field name
            ),
            $this->datagridValues
        );
        return parent::getFilterParameters();
    }
}
