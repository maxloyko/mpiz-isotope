<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

class IsotopeAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('active')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('wideName')
            ->add('active')
            ->add('full_isotope', 'string', array(
                'template' => 'IsotopeDbBundle:Admin:list_full_isotope_mci.html.twig'))
            ->add('_action', 'actions', array(
                'actions' => array(
                    //'show' => array(),
                    'edit' => array(),
                    'hide' => array(
                        'template' => 'IsotopeDbBundle:Admin:hide__action_isotope.html.twig'
                    ),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Isotop')
            ->add('category',null,array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Category',
               'required' => true,))
            ->add('catalogNumber', null, array('required' => true))
            ->add('rounding', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => false))
            ->add('highestAmount', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => false))
            ->add('company', null, array('required' => false))
            ->add('productLine', null, array('required' => false))
            ->add('substance', null, array('required' => false))
            ->add('specificActivity', null, array('required' => false))
            ->add('concentration', null, array('required' => false))
            ->add('deliveryType', null,array('required' => true))
            ->add('note', null, array('required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('category')
            ->add('catalogNumber')
            ->add('rounding', null, array(
                'template' => 'IsotopeDbBundle:Admin:show_rounding.html.twig',
                ))
            ->add('highestAmount', null, array(
                'template' => 'IsotopeDbBundle:Admin:show_highest_amount.html.twig',
                ))
            ->add('company')
            ->add('productLine')
            ->add('substance')
            ->add('specificActivity')
            ->add('concentration')
            ->add('deliveryType')
            ->add('note')
            ->add('active')
        ;
    }

    public function getBatchActions()
    {
        return null;
    }

    protected function configureRoutes(RouteCollection $collection) {
        $collection->add('hide', $this->getRouterIdParameter().'/hide');
        $collection->add('reactivate', $this->getRouterIdParameter().'/reactivate');
        $collection->remove('delete');
    }

    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('category')
                ->assertNotBlank()
            ->end()
            ->with('catalogNumber')
                ->assertNotBlank()
            ->end()
            ->with('deliveryType')
                ->assertNotBlank()
            ->end()
            ->with('rounding')
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
            ->with('highestAmount')
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
        ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'hide':
                return 'IsotopeDbBundle:Admin:hide.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
