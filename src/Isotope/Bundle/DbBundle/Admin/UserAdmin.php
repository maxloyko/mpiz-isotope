<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;

class UserAdmin extends Admin
{
    public function postUpdate($object)
    {
        $lang = array('en', 'de');

        if (in_array( $object->getLocale(), $lang)) {
            $this->getRequest()->getSession()->set('_locale', $object->getLocale());
        }

    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username'/*, null, array('label' => 'Benutzername')*/)
            ->add('email'/*, null, array('label' => '')/*/)
            ->add('enabled')
            ->add('locked')
            ->add('expired')
            ->add('credentialsExpired')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        
        $listMapper
            ->add('username')
            ->add('first_name')
            ->add('last_name')
            ->add('phone')
            ->add('email')
            ->add('enabled')
            ->add('room_number')
            ->add('roles', null, array('template' => 'IsotopeDbBundle:Admin:list_user_role.html.twig',
                'role' => true))
            ->add('_action', 'actions', array(/*'label' => 'Aktionen',*/
                'actions' => array(
                    'show' => array(),
                    'disable' => array('template' => 'IsotopeDbBundle:Admin:list__action_user_disable.html.twig')
                    )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('username')
            ->add('usernameCanonical', null, array('required' => false))
            ->add('email', null, array('required' => false))
            ->add('emailCanonical', null, array('required' => false))
            ->add('enabled', null, array('required' => false))
            ->add('salt', null, array('required' => false))
            ->add('password', null, array('required' => false))
            ->add('lastLogin','date',array(
                'required' => false,
                'read_only' => true,
                'disabled' => true,
                'widget' => 'single_text',
                'format' => "MMM d, y, H:mm:ss"
                ))
            ->add('locked', null, array('required' => false))
            ->add('expired', null, array('required' => false))
            ->add('confirmationToken', null, array('required' => false))
            ->add('passwordRequestedAt','date',array(
                'required' => false,
                'read_only' => true,
                'disabled' => true,
                'widget' => 'single_text',
                'format' => "MMM d, y, H:mm:ss"))
            ->add('roles', null, array('required' => false))
            ->add('credentialsExpired', null, array('required' => false))
            ->add('dn', null, array('required' => false))
            ->add('locale', 'choice', array('choices'   => array(
                'en'   => 'En',
                'de' => 'De',
            ),))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('username')
            ->add('first_name')
            ->add('last_name')
            ->add('phone')
            ->add('email')
            ->add('enabled')
            ->add('room_number')
            ->add('roles', null, array('template' => 'IsotopeDbBundle:Admin:show_user_role.html.twig',
                'role' => true))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create')
                ->remove('edit')
                ->remove('delete')
                ->add('enable', $this->getRouterIdParameter() . '/enable')
                ->add('disable', $this->getRouterIdParameter() . '/disable')
                ;
    }

    public function getBatchActions()
    {
        return null;
    }
    /**
     * Default Datagrid values
     *
     * @return array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
                '_sort_order' => 'DESC', // sort direction
                '_sort_by' => 'id' // field name
            ),
            $this->datagridValues
        );
        return parent::getFilterParameters();
    }
}
