<?php
/**
 * Created by PhpStorm.
 * User: maxloyko
 * Date: 1/23/14
 * Time: 4:01 PM
 */

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

class MonthlyReportSettingAdmin extends Admin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Monatsbericht Einstellungen')
            ->add('address','textarea', array('label' => 'Adresse', 'required' => true))
            ->add('subject', 'text', array('label' => 'Betreff','required' => true))
            ->add('content', 'textarea', array('label' => 'Inhalt','required' => true))
            ->add('mpizAddres', 'textarea', array('label' => 'MPIZ-Adresse','required' => true))
        ;
    }
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('edit'));
    }
} 