<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route as Route;

use Doctrine\ORM\EntityRepository as Repository;
use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

class CurrentOrderAdmin extends Admin
{
    protected $baseRouteName = 'admin_isotope_db_currentorder';

    protected $baseRoutePattern = '/isotope/db/currentorder';

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $ldap_config = array_keys( $this->getConfigurationPool()->getContainer()->getParameter('ldap_fields'));
        $listMapper
            ->add('isotope', 'string', array('template' => 'IsotopeDbBundle:Admin:list_full_isotope_order_mci.html.twig'))
            ->add('date',null,array('sortable' => true,'format' => 'd.m.Y'))
            ->add('isotope.category')
            ->add('amount',null,array(
                'sortable' => true,
                'template' => 'IsotopeDbBundle:Admin:list_amount.html.twig',
                ))
            ->add('statusLabel', null, array('template' => 'IsotopeDbBundle:Admin:list_order_status_trans.html.twig'))
                ->add('user', null, array('template' => 'IsotopeDbBundle:Admin:list_username_order.html.twig',
                'ldap_fields' => $ldap_config))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array('template' => 'IsotopeDbBundle:Admin:show__action_order_mci.html.twig'),
                    'edit' => array(),
                    'delete' => array('template' => 'IsotopeDbBundle:Admin:remove__action_currentorder.html.twig'),
                )
            ))
        ;
    }

    /**
     * Customising the query used to generate context
     * @param string, default is `list`
     * @return query
     */

    public function createQuery($context = 'list')
    {
        $user = $this->getCurrentUser();

        $query = parent::createQuery($context);
        if ($this->getRequest()->get('_route') == 'admin_isotope_db_currentorder_list') {
            $query->where(
                $query->expr()->eq($query->getRootAlias()  . '.status', ':status')
            );
            $query->setParameter('status', 'order');
            $query->andWhere(
                $query->expr()->eq($query->getRootAlias()  . '.visible', ':visible')
            );
            $query->setParameter('visible', true);
        }
        return $query;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        if (!$this->id($this->getSubject())) {
            $request = $this->getRequest();
            $isotope_id = $request->get('isotope');

            if (is_numeric($isotope_id) && is_int($isotope_id + 0)) {
                $rep = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('Isotope\Bundle\DbBundle\Entity\Isotope');

                $isotope = $rep->findOneById($isotope_id);

                $this->getSubject()->setIsotope($isotope);
            }
        }

        $user = $this->getCurrentUser();
        $this->getSubject()->setUser($user);
        $request = $this->getConfigurationPool()->getContainer()->get('request');
        $locale = $request->getLocale();
        $rename_label = $locale == 'en' ? 'Order' : 'Bestellen';
        $rename_update_label = $locale == 'en' ? 'Update' : 'Speichern';

        $formMapper
            ->with('order', array('translation_domain' => 'messages'))
                ->add('isotope','entity',array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Isotope',
                'expanded' => true,
                'compound' => true,
                'property' => 'wideName',
                'query_builder' => function (Repository $repository) {
                        return $repository->createQueryBuilder('e')
                            ->where('e.active = true')
                            ->orderBy('e.substance', 'ASC')
                            ->addOrderBy('e.id', 'ASC');
                    },
                'attr' => array('data-popup' => 'show', 'class' => 'isotope_order_list'),
                'required' => true))
            ->add('amount', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => true))
            ->add('costCenter', null, array('required' => true,
                'attr'     => array('class' => 'span5 order_cost_center')))
            ->add('formDesignSetting', 'hidden', array('required' => false, 'mapped' => false, 'attr' => array('class' => 'currentorder_form_design_settings', 'data-setting' => "hide: btn_create_and_edit,hide:btn_create_and_create,active: btn_create_and_list,rename_label: $rename_label,rename: btn_create_and_list,hide:btn_update_and_edit,active: btn_update_and_list,rename_label: $rename_update_label,rename: btn_update_and_list")))
        ;
    }

    public function prePersist($order)
    {
        $user = $this->getCurrentUser();
        $order->setUser($user);
    }

    public function preLoad($order)
    {
        $userManager = $this->getUserManager();
        $securityContext = $this->getSecurityContext();
        $username = $securityContext->getToken()->getUser();
        $user = $userManager->findUserByUsername($username);
        $order->setUser($user);
    }

    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function getSecurityContext()
    {
        return $this->securityContext;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('date',null, array('format' => 'd.m.Y'))
            ->add('stockDate',null, array('format' => 'd.m.Y'))
            ->add('isotope', null, array('template' => 'IsotopeDbBundle:Admin:show_full_isotope_order_mci.html.twig'))
            ->add('amount', null, array(
                'template' => 'IsotopeDbBundle:Admin:show_amount.html.twig',
                ))
            ->add('status')
            ->add('disposed')
            ->add('costCenter')
        ;
    }

    public function getBatchActions()
    {
        return null;
    }

    /**
     * @return object current logged user
     */
    private function getCurrentUser()
    {
        $userManager = $this->getUserManager();
        $securityContext = $this->getSecurityContext();
        $username = $securityContext->getToken()->getUser();
        $user = $userManager->findUserByUsername($username);

        return $user;
    }

    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('isotope')
                ->assertNotBlank(array('message' => 'You have to select an isotope for your order.'))
            ->end()
            ->with('amount')
                ->assertNotBlank()
                ->assertGreaterThan(array('value' => 0, 'message' => 'Amount has to be greater than 0.'))
            ->end()
            ->with('costCenter')
                ->assertNotBlank()
            ->end()
        ;
        $order = $this->getSubject();
        if($order->getIsotope()){
            $limit = $order->getIsotope()->getHighestAmount();
            $limitUci = $order->getIsotope()->getHighestAmountUci();
            if ($limit > 0) {
                $errorElement
                        ->with('amount')
                        ->assertLessThanOrEqual(array('value' => $limit, 'message' => "You may not order more than $limitUci µCi of this isotope."))
                        ->end();
            }
        }
    }

    public function isGranted($name, $object = null) {
        switch ($name) {
            case 'DELETE':
                return $this->getCurrentUser() == $object->getUser();
                break;
            case 'EDIT':
                return $this->getCurrentUser() == $object->getUser();
                break;
        }

        return parent::isGranted($name, $object);
    }

      protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'list':
                return 'IsotopeDbBundle:Admin:base_list.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
