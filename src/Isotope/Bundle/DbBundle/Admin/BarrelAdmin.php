<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

class BarrelAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('barrelsCategory')
            ->add('description')
            ->add('visible')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('barrelsCategory')
            ->add('description')
            ->add('visible')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'hide' => array('template' => 'IsotopeDbBundle:Admin:remove__action_barrel.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Tonne')
            ->add('barrelsCategory')
            ->add('description',null,array('required' => false))
            //->add('visible', null, array('required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('barrelsCategory')
            ->add('description')
            ->add('visible')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('reactivate', $this->getRouterIdParameter().'/reactivate');
        $collection->add('hide', $this->getRouterIdParameter().'/hide');
        $collection->remove('delete');

    }

    /**
     * Default Datagrid values
     *
     * @var array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            'visible' => array(
                'value' => 1,
            ),
            '_sort_order' => 'DESC', // sort direction
            '_sort_by' => 'id' // field name
        ),
        $this->datagridValues

    );
    return parent::getFilterParameters();
    }

    public function getBatchActions()
    {
        return null;
    }

   /* public function preRemove($barrel)
    {
        $barrel->setVisible(FALSE);
        $em = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager();
        $em->persist($barrel);
        $em->flush();
        //$this->getConfigurationPool()->getContainer()->get('session')->setFlash('sonata_flash_success', 'flash_batch_delete_success');
    }*/

    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('barrelsCategory')
                ->assertNotBlank()
            ->end()
        ;
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'hide':
                return 'IsotopeDbBundle:Admin:hide.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
