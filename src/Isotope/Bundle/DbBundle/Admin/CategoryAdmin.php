<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;

class CategoryAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('halfLife')
            ->add('releaseLimit')
            ->add('internalDisposal')
            ->add('visible')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name')
            ->add('halfLife')
            ->add('releaseLimit', 'integer', array(
                'template' => 'IsotopeDbBundle:Admin:list__action_category.html.twig'
            ))
            ->add('internalDisposal')
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'hide' => array('template' => 'IsotopeDbBundle:Admin:remove__action_category.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Kategorie')
//            ->add('id')
            ->add('name',null, array('required' => true))
            ->add('halfLife', null, array('required' => true))
            ->add('releaseLimit', null, array(
                'required' => false,
                'label_attr' => array(
                    'class' => 'releaseLimit required'
                ),
                'attr' => array(
                    'class'=>'releaseLimit'
                )
            ))
            ->add('internalDisposal', null, array(
                'required' => false,
                'attr' => array(
                    'class'=>'internalDisposal'
                )
            ))
            //->add('visible', null, array('required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('halfLife')
            ->add('releaseLimit', 'integer', array(
                'template' => 'IsotopeDbBundle:Admin:show__action_category.html.twig'
            ))
            ->add('internalDisposal')
            ->add('visible')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('reactivate', $this->getRouterIdParameter().'/reactivate');
        $collection->add('hide', $this->getRouterIdParameter().'/hide');
        $collection->remove('delete');

    }

    /**
     * Default Datagrid values
     *
     * @var array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            'visible' => array(
                'value' => 1,
            ),
            '_sort_order' => 'DESC', // sort direction
            '_sort_by' => 'id' // field name
        ),
        $this->datagridValues

    );
    return parent::getFilterParameters();
    }

    public function getBatchActions()
    {
        return null;
    }

    public function validate(ErrorElement $errorElement, $category)
    {
        $errorElement
            ->with('name')
                
                ->assertNotBlank()
            ->end()
            ->with('halfLife')
                ->assertNotBlank()
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
            ->with('releaseLimit')
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
        ;

        if ($category->getInternalDisposal()) {
            $errorElement
                ->with('releaseLimit')
                    ->assertNotBlank()
                ->end();
        }
    }

    public function getTemplate($name)
    {
        switch ($name) {
            case 'hide':
                return 'IsotopeDbBundle:Admin:hide.html.twig';
                break;
            default:
                return parent::getTemplate($name);
                break;
        }
    }
}
