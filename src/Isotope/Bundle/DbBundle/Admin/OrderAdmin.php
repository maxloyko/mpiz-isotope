<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route as Route;

use Doctrine\ORM\EntityRepository as Repository;

use FOS\UserBundle\Model\UserManagerInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\DoctrineORMAdminBundle\Datagrid\ProxyQuery;

class OrderAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('user')
            ->add('date')
            ->add('stockDate')
            ->add('isotope')
            ->add('isotope.category',null,array())
            ->add('status', null, array(), 'choice', array('choices' => array('order' => 'Bestellungen', 'stock' => 'Bestand')))
            ->add('visible')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $ldap_config = array_keys( $this->getConfigurationPool()->getContainer()->getParameter('ldap_fields'));
        $listMapper
            ->add('isotope', 'string', array('template' => 'IsotopeDbBundle:Admin:list_full_isotope_order_mci.html.twig'))
            ->add('date',null,array('sortable' => true,'format' => 'd.m.Y'))
            ->add('stockDate',null,array('sortable' => true,'format' => 'd.m.Y'))
            ->add('isotope.category')
            ->add('amount',null,array(
                'sortable' => true,
                'template' => 'IsotopeDbBundle:Admin:list_amount.html.twig',
                ))
            ->add('statusLabel', null, array('template' => 'IsotopeDbBundle:Admin:list_order_status_trans.html.twig'))
                ->add('user', null, array('template' => 'IsotopeDbBundle:Admin:list_username_order.html.twig',
                'ldap_fields' => $ldap_config))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array('template' => 'IsotopeDbBundle:Admin:show__action_order_mci.html.twig'),
                    'edit' => array(),
                    'delete' => array('template' => 'IsotopeDbBundle:Admin:remove__action_order.html.twig'),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        if (!$this->id($this->getSubject())) {
            $request = $this->getRequest();
            $isotope_id = $request->get('isotope');

            if (is_numeric($isotope_id) && is_int($isotope_id + 0)) {
                $rep = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('Isotope\Bundle\DbBundle\Entity\Isotope');

                $isotope = $rep->findOneById($isotope_id);

                $this->getSubject()->setIsotope($isotope);
            }
        }

        $user = $this->getCurrentUser();
        $this->getSubject()->setUser($user);

        $formMapper
            ->with('Bestellung')
            ->add('isotope','entity',array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Isotope',
                'expanded' => true,
                'compound' => true,
                'property' => 'wideName',
                'query_builder' => function (Repository $repository) {
                        return $repository->createQueryBuilder('e')
                            ->where('e.active = true')
                            ->orderBy('e.substance', 'ASC')
                            ->addOrderBy('e.id', 'ASC');
                    },
                'attr' => array('data-popup' => 'show', 'class' => 'isotope_order_list'),
                'required' => true))
            ->add('amount', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => true))
            ->add('costCenter', null, array('required' => true,
                'attr' => array('class' => 'span5 order_cost_center')))
            ->add('status', 'hidden', array('required' => false,'disabled' => true,
                'attr' => array('class' => 'span5 order_status',"hidden" => true)))
        ;
    }

    public function prePersist($order)
    {
        $user = $this->getCurrentUser();
        $order->setUser($user);
    }

    public function setUserManager(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }

    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function getSecurityContext()
    {
        return $this->securityContext;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
//            ->add('id')
//            ->add('user')
            ->add('date', null, array('format' => 'd.m.Y'))
            ->add('stockDate', null, array('format' => 'd.m.Y'))
            ->add('isotope', null, array('template' => 'IsotopeDbBundle:Admin:show_full_isotope_order_mci.html.twig'))
            ->add('amount', null, array(
                'template' => 'IsotopeDbBundle:Admin:show_amount.html.twig',
                ))
            ->add('status')
            ->add('disposed')
            ->add('costCenter')
//            ->add('visible')
        ;
    }

   // protected function configureRoutes(RouteCollection $collection)
   // {
        /*$collection
            ->remove('delete')
            ;*/

    //}

    /**
     * Default Datagrid values
     *
     * @var array
     */
    public function getFilterParameters()
    {
        $this->datagridValues = array_merge(array(
            'visible' => array(
                'value' => 1,
            ),
            '_sort_order' => 'DESC', // sort direction
            '_sort_by' => 'date' // field name
        ),
        $this->datagridValues

    );
    return parent::getFilterParameters();
    }

    public function getBatchActions()
    {
        return null;
    }

    /**
     * @return object current logged user
     */
    private function getCurrentUser()
    {
        $userManager = $this->getUserManager();
        $securityContext = $this->getSecurityContext();
        $username = $securityContext->getToken()->getUser();
        $user = $userManager->findUserByUsername($username);

        return $user;
    }

    protected function configureRoutes(Route\RouteCollection $collection) {
        $collection
            ->remove('create');
    }

    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('isotope')
                ->assertNotBlank()
            ->end()
            ->with('amount')
                ->assertNotBlank()
                ->assertGreaterThan(array('value' => 0, 'message' => 'Amount has to be greater than 0.'))
            ->end()
            ->with('costCenter')
                ->assertNotBlank()
            ->end()
        ;
        $order = $this->getSubject();
        $limit = $order->getIsotope()->getHighestAmount();
        $limitUci = $order->getIsotope()->getHighestAmountUci();
        if($limit > 0) {
            $errorElement
                ->with('amount')
                    ->assertLessThanOrEqual(array('value' => $limit, 'message' => "You may not order more than $limitUci of this isotope."))
                ->end();
        }
    }
}
