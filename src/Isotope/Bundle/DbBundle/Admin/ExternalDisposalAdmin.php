<?php

namespace Isotope\Bundle\DbBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route as Route; 
use Sonata\AdminBundle\Validator\ErrorElement;

class ExternalDisposalAdmin extends Admin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('category')
            ->add('company')
            ->add('date')
            ->add('amount')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('category')
            ->add('company')
            ->add('date', null, array('format' => 'd.m.Y'))
            ->add('amount', null, array(
                'sortable' => true,
                'template' => 'IsotopeDbBundle:Admin:list_amount.html.twig',
                ))
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Externe Entsorgung')
            ->add('category',null,array(
                'class' => 'Isotope\Bundle\DbBundle\Entity\Category',
                'required' => true,
                'attr' => array('data-attach' => 'category_amount')))
            ->add('company', null, array('required' => true))
            ->add('date', null, array('required' => true))
            ->add('amount', null, array(
                'attr' => array('data-convert' => 'bq', 'class' => 'amount_convert'),
                'required' => true))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('category')
            ->add('company')
            ->add('date', null, array('required' => true,'format' => 'd.m.Y'))
            ->add('amount', null, array(
                'template' => 'IsotopeDbBundle:Admin:show_amount.html.twig',
                ))
        ;
    }

    public function getBatchActions()
    {
        return null;
    }

    protected function configureRoutes(Route\RouteCollection $collection)
    {
        $collection->add('approve', $this->getRouterIdParameter() . '/approve');
        $collection->remove('edit');
    }

    public function getNewInstance()
    {
        $disposal = parent::getNewInstance();
        $disposal->setDate( new \DateTime( 'today' ) );

        return $disposal;
    }
    
    public function validate(ErrorElement $errorElement, $isotope)
    {
        $errorElement
            ->with('category')
                ->assertNotBlank()
            ->end()    
            ->with('company')
                ->assertNotBlank()
            ->end()
            ->with('amount')
                ->assertNotBlank()
                ->assertGreaterThanOrEqual(array('value' => 0, 'message' => 'Positive value only suitable.'))
            ->end()
        ;
    }
}
